package org.meveo.apiv2.catalog.resource.pricelist;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.meveo.api.exception.BusinessApiException;
import org.meveo.apiv2.catalog.ImmutablePriceList;
import org.meveo.apiv2.generic.ResourceMapper;
import org.meveo.commons.utils.EjbUtils;
import org.meveo.commons.utils.StringUtils;
import org.meveo.model.admin.Currency;
import org.meveo.model.admin.Seller;
import org.meveo.model.billing.Country;
import org.meveo.model.crm.CustomerBrand;
import org.meveo.model.crm.CustomerCategory;
import org.meveo.model.jaxb.customer.CustomFields;
import org.meveo.model.payments.CreditCategory;
import org.meveo.model.payments.PaymentMethodEnum;
import org.meveo.model.pricelist.PriceList;
import org.meveo.model.pricelist.PriceListStatusEnum;
import org.meveo.model.shared.Title;
import org.meveo.service.api.EntityToDtoConverter;

public class PriceListMapper extends ResourceMapper<org.meveo.apiv2.catalog.PriceList, PriceList> {

	private EntityToDtoConverter entityToDtoConverter = (EntityToDtoConverter) EjbUtils.getServiceInterface(EntityToDtoConverter.class.getSimpleName());

	@Override
	protected org.meveo.apiv2.catalog.PriceList toResource(PriceList entity) {
		return ImmutablePriceList.builder()
				.id(entity.getId())
				.code(entity.getCode())
				.description(entity.getDescription())
				.validFrom(entity.getValidFrom())
				.validUntil(entity.getValidUntil())
				.applicationStartDate(entity.getApplicationStartDate())
				.applicationEndDate(entity.getApplicationEndDate())
				.status(getStatus(entity))
				.brands(getBrandsCodes(entity))
				.customerCategories(getCustomerCategoriesCodes(entity))
				.creditCategories(getCreditCategoriesCodes(entity))
				.countries(getCountriesCodes(entity))
				.currencies(getCurrenciesCodes(entity))
				.legalEntities(getLegalEntitiesCodes(entity))
				.sellers(getSellersCodes(entity))
				.paymentMethods(getPaymentMethodCode(entity))
				.customFields(entityToDtoConverter.getCustomFieldsDTO(entity))
				.build();	
    }
	
	public org.meveo.apiv2.catalog.PriceList toResource(PriceList entity, boolean b) {
		return this.toResource(entity);
	}
	
	/**
	 * Get codes from a set of Brands
	 * @param entity Price List
	 * @return a list of brands codes
	 */
	private List<String> getBrandsCodes(PriceList entity) {
		if (entity == null || entity.getBrands() == null) {
			return null;
		}
		return entity.getBrands().stream().map(brand -> brand.getCode()).collect(Collectors.toList());
	}
	
	/**
	 * Get codes from a set of Customer Categories
	 * @param entity Price List
	 * @return a list of customer categories
	 */
	private List<String> getCustomerCategoriesCodes(PriceList entity) {
		if (entity == null || entity.getCustomerCategories() == null) {
			return null;
		}
		return entity.getCustomerCategories().stream().map(customerCategory -> customerCategory.getCode()).collect(Collectors.toList());
	}
	
	/**
	 * Get codes from a set of Credit Categories
	 * @param entity Price List
	 * @return a list of Credit Categories
	 */
	private List<String> getCreditCategoriesCodes(PriceList entity) {
		if (entity == null || entity.getCreditCategories() == null) {
			return null;
		}
		return entity.getCreditCategories().stream().map(creditCategory -> creditCategory.getCode()).collect(Collectors.toList());
	}
	
	/**
	 * Get codes from a set of Countries
	 * @param entity Price List
	 * @return a list of Countries
	 */
	private List<String> getCountriesCodes(PriceList entity) {
		if (entity == null || entity.getCountries() == null) {
			return null;
		}
		return entity.getCountries().stream().map(country -> country.getCode()).collect(Collectors.toList());
	}
	
	/**
	 * Get codes from a set of Currencies
	 * @param entity Price List
	 * @return a list of Currencies
	 */
	private List<String> getCurrenciesCodes(PriceList entity) {
		if (entity == null || entity.getCurrencies() == null) {
			return null;
		}
		return entity.getCurrencies().stream().map(currency -> currency.getCurrencyCode()).collect(Collectors.toList());
	}
	
	/**
	 * Get codes from a set of Legal Entities
	 * @param entity Price List
	 * @return a list of Legal Entities
	 */
	private List<String> getLegalEntitiesCodes(PriceList entity) {
		if (entity == null || entity.getLegalEntities() == null) {
			return null;
		}
		return entity.getLegalEntities().stream().map(legalEntity -> legalEntity.getCode()).collect(Collectors.toList());
	}
	
	/**
	 * Get codes from a set of Sellers
	 * @param entity Price List
	 * @return a list of Sellers
	 */
	private List<String> getSellersCodes(PriceList entity) {
		if (entity == null || entity.getSellers() == null) {
			return null;
		}
		return entity.getSellers().stream().map(seller -> seller.getCode()).collect(Collectors.toList());
	}
	
	/**
	 * Get codes from a set of Payment Methods
	 * @param entity Price List
	 * @return a list of Payment Methods
	 */
	private List<String> getPaymentMethodCode(PriceList entity) {
		if (entity == null || entity.getPaymentMethods() == null) {
			return null;
		}
		return entity.getPaymentMethods().stream().map(paymentMethod -> paymentMethod.name()).collect(Collectors.toList());
	}
	
	/**
	 * Get status from a set enumeration
	 * @param entity Price List
	 * @return a status name
	 */
	private String getStatus(PriceList entity) {
		if (entity == null || entity.getStatus() == null) {
			return null;
		}
		return entity.getStatus().name();
	}

	@Override
	protected PriceList toEntity(org.meveo.apiv2.catalog.PriceList resource) {
		var entity = new PriceList();		
		entity.setId(resource.getId());
		entity.setCode(resource.getName() != null ? resource.getName() : resource.getCode());
        entity.setDescription(resource.getDescription());
        entity.setValidFrom(resource.getValidFrom());
        entity.setValidUntil(resource.getValidUntil());
        entity.setApplicationStartDate(resource.getApplicationStartDate());
        entity.setApplicationEndDate(resource.getApplicationEndDate());
        entity.setStatus(getStatus(resource.getStatus()));
        entity.setBrands(getBrandsFromCodes(resource.getBrands()));
        entity.setCustomerCategories(getCustomerCategoriesFromCodes(resource.getCustomerCategories()));
        entity.setCountries(getCountriesFromCodes(resource.getCountries()));
        entity.setCreditCategories(getCreditCategoriesFromCodes(resource.getCreditCategories()));
        entity.setCurrencies(getCurrenciesFromCodes(resource.getCurrencies()));
        entity.setLegalEntities(getLegalEntitiesFromCodes(resource.getLegalEntities()));
        entity.setSellers(getSellersFromCodes(resource.getSellers()));
        entity.setPaymentMethods(getPaymentMethodsFromCodes(resource.getPaymentMethods()));
        return entity;
	}

	/**
	 * Get a set of {@link CustomerBrand} from codes
	 * @param customerBrandCodes customer brand codes
	 * @return Set of {@link CustomerBrand}
	 */
	private Set<CustomerBrand> getBrandsFromCodes(List<String> customerBrandCodes) {
		if (customerBrandCodes == null) {
			return null;
		}
		
		return customerBrandCodes.stream().map(code -> {
			CustomerBrand customerBrand = new CustomerBrand();
			customerBrand.setCode(code);
			return customerBrand;
		}).collect(Collectors.toSet());
	}
	
	/**
	 * Get a set of {@link CustomerCategory} from codes
	 * @param customerCategoryCodes customer category codes
	 * @return Set of {@link CustomerCategory}
	 */
	private Set<CustomerCategory> getCustomerCategoriesFromCodes(List<String> customerCategoryCodes) {
		if (customerCategoryCodes == null) {
			return null;
		}

		return customerCategoryCodes.stream().map(code -> {
			CustomerCategory customerCategory = new CustomerCategory();
			customerCategory.setCode(code);
			return customerCategory;
		}).collect(Collectors.toSet());
	}
	
	/**
	 * Get a set of {@link Country} from codes
	 * @param countryCodes country category codes
	 * @return Set of {@link Country}
	 */
	private Set<Country> getCountriesFromCodes(List<String> countryCodes) {
		if (countryCodes == null) {
			return null;
		}

		return countryCodes.stream().map(code -> {
			Country country = new Country();
			country.setCountryCode(code);
			country.setCode(code);
			return country;
		}).collect(Collectors.toSet());
	}
	
	/**
	 * Get a set of {@link CreditCategory} from codes
	 * @param creditCategoriesCodes credit category codes
	 * @return Set of {@link CreditCategory}
	 */
	private Set<CreditCategory> getCreditCategoriesFromCodes(List<String> creditCategoriesCodes) {
		if (creditCategoriesCodes == null) {
			return null;
		}

		return creditCategoriesCodes.stream().map(code -> {
			CreditCategory creditCategory = new CreditCategory();
			creditCategory.setCode(code);
			return creditCategory;
		}).collect(Collectors.toSet());
	}
	
	/**
	 * Get a set of {@link Currency} from codes
	 * @param currenciesCodes currency codes
	 * @return Set of {@link Currency}
	 */
	private Set<Currency> getCurrenciesFromCodes(List<String> currenciesCodes) {
		if (currenciesCodes == null) {
			return null;
		}

		return currenciesCodes.stream().map(code -> {
			Currency currency = new Currency();
			currency.setCurrencyCode(code);
			return currency;
		}).collect(Collectors.toSet());
	}
	
	/**
	 * Get a set of {@link Title} from codes
	 * @param legalEntitiesCodes legal entity codes
	 * @return Set of {@link Title}
	 */
	private Set<Title> getLegalEntitiesFromCodes(List<String> legalEntitiesCodes) {
		if (legalEntitiesCodes == null) {
			return null;
		}

		return legalEntitiesCodes.stream().map(code -> {
			Title title = new Title();
			title.setCode(code);
			return title;
		}).collect(Collectors.toSet());
	}
		
	/**
	 * Get a set of {@link Seller} from codes
	 * @param sellersCodes seller codes
	 * @return Set of {@link Seller}
	 */
	private Set<Seller> getSellersFromCodes(List<String> sellersCodes) {
		if (sellersCodes == null) {
			return null;
		}
	
		return sellersCodes.stream().map(code -> {
			Seller seller = new Seller();
			seller.setCode(code);
			return seller;
		}).collect(Collectors.toSet());
	}
	
	/**
	 * Get a set of {@link PaymentMethodEnum}
	 * @param paymentMethods Payment method codes
	 * @return Set of {@link PaymentMethodEnum}
	 */
	private Set<PaymentMethodEnum> getPaymentMethodsFromCodes(Set<String> paymentMethods) {
		if (paymentMethods != null) {
    		return paymentMethods.stream().filter(StringUtils::isNotBlank).map(PaymentMethodEnum::valueOf).collect(Collectors.toSet());
        }
		
		return null;
	}
	
	/**
	 * Get Status 
	 * @param status
	 * @return {@link PriceListStatusEnum}
	 */
	private PriceListStatusEnum getStatus(String status) {
		if(status == null) {
			return null;
		} else if(status.equals(PriceListStatusEnum.DRAFT.name()) || status.equals(PriceListStatusEnum.ACTIVE.name()) ||
				status.equals(PriceListStatusEnum.CLOSED.name()) || status.equals(PriceListStatusEnum.ARCHIVED.name())) {
			return PriceListStatusEnum.valueOf(status);			
		} else {
			throw new BusinessApiException("The price list status can be either Draft, Active, Closed, or Archived");
		}
	}
}