package org.meveo.model.ordering;

public enum OpenOrderTemplateStatusEnum {

    DRAFT, ARCHIVED, ACTIVE
}