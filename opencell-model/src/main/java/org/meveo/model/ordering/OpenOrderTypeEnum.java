package org.meveo.model.ordering;

public enum OpenOrderTypeEnum {

    ARTICLES, PRODUCTS
}
