package org.meveo.model.ordering;

public enum OpenOrderQuoteStatusEnum {

	 DRAFT, WAITING_VALIDATION, REJECTED, VALIDATED, SENT, ACCEPTED, CANCELED
}