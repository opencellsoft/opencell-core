package org.meveo.model.ordering;

public enum OpenOrderStatusEnum {
    NEW, IN_USE, EXPIRED, SOLD_OUT, CANCELED
}