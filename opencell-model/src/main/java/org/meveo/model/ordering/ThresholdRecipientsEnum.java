package org.meveo.model.ordering;

public enum ThresholdRecipientsEnum {

    SALES_AGENT, CUSTOMER, CONSUMER
}
