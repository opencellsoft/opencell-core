package org.meveo.model.cpq.commercial;

public enum OfferLineTypeEnum {
    CREATE, AMEND, TERMINATE, APPLY_ONE_SHOT
}
