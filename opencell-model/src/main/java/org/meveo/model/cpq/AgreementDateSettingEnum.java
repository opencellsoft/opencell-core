package org.meveo.model.cpq;

/**
 * Define the strategy for setting the end date.
 * 
 * @author anas
 *
 */
public enum AgreementDateSettingEnum {

	COPY, INHERIT, MANUAL;
}
