package org.meveo.model.cpq.commercial;

public enum ProductActionTypeEnum {
    CREATE, ACTIVATE, SUSPEND, TERMINATE, MODIFY
}
