package org.meveo.model.order;

public enum OrderLineTypeEnum {
	CREATE, AMEND, TERMINATE, APPLY_ONE_SHOT
}

