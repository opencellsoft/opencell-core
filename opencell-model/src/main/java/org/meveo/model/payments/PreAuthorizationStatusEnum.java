package org.meveo.model.payments;

public enum PreAuthorizationStatusEnum {
	 AUTORISED, CAPTURED, CANCELED,REJECTED
}
