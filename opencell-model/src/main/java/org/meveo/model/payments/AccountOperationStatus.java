package org.meveo.model.payments;

public enum AccountOperationStatus {
    POSTED, REJECTED, EXPORTED, EXPORT_FAILED, CLOSED;
}