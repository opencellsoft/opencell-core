package org.meveo.model.payments;

public enum DefaultStartingDateOfPlan {
    TODAY,
    LAST_DAY_OF_CURRENT_MONTH,
    FIRST_DAY_OF_NEXT_MONTH
}
