package org.meveo.model.payments;

public enum MandateChangeAction {
    NONE, TO_ADVERTISE, ADVERTISED
}
