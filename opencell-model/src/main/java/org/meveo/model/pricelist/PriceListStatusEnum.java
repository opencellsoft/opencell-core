package org.meveo.model.pricelist;

public enum PriceListStatusEnum {

    DRAFT,
    ACTIVE,
    CLOSED,
    ARCHIVED;

}
