package org.meveo.model.pricelist;

/**
 * PriceList Tupes enumarator
 *
 * @author a.rouaguebe
 */
public enum PriceListTypeEnum {
    FIXED, PERCENTAGE
}
