package org.meveo.model.billing;

public enum PDPStatusEnum {
	DEPOSITED, CASHED, RECTIFICATIVE, REJECTED, DISPUTED
}
