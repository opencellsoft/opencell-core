package org.meveo.model.billing;

public enum TransmittedFormatEnum {
	UBL, XML, PDF
}
