package org.meveo.model.billing;

public enum InvoiceLineTaxModeEnum {
    /**
     * Taxe from the Invoice.
     */
    ARTICLE,
    /**
     * Choice from Taxe.
     */
    TAX,
    /**
     * Manuel Rate.
     */
    RATE
}
