/*
 * (C) Copyright 2015-2020 Opencell SAS (https://opencellsoft.com/) and contributors.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW. EXCEPT WHEN
 * OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS
 * IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE ENTIRE RISK AS TO
 * THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU. SHOULD THE PROGRAM PROVE DEFECTIVE,
 * YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.
 *
 * For more information on the GNU Affero General Public License, please consult
 * <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */
package org.meveo.model.billing;

import static javax.persistence.FetchType.LAZY;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.meveo.model.CustomFieldEntity;
import org.meveo.model.EnableEntity;
import org.meveo.model.ICustomFieldEntity;
import org.meveo.model.IReferenceEntity;
import org.meveo.model.ObservableEntity;
import org.meveo.model.ReferenceIdentifierQuery;
import org.meveo.model.admin.Currency;
import org.meveo.model.admin.User;
import org.meveo.model.crm.custom.CustomFieldValues;
import org.meveo.model.jobs.JobExecutionResultImpl;

/**
 * Billing run
 * 
 * @author Andrius Karpavicius
 * @author Abdellatif BARI
 * @lastModifiedVersion 7.0
 */
@Entity
@ObservableEntity
@ReferenceIdentifierQuery("BillingRun.findByIdAndBCCode")
@CustomFieldEntity(cftCodePrefix = "BillingRun")
@Table(name = "billing_billing_run")
@GenericGenerator(name = "ID_GENERATOR", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = { @Parameter(name = "sequence_name", value = "billing_billing_run_seq") })
@NamedQueries({
        @NamedQuery(name = "BillingRun.getForInvoicing", query = "SELECT br FROM BillingRun br where br.status in ('NEW', 'PREVALIDATED', 'INVOICES_GENERATED', 'POSTVALIDATED') or (br.status='POSTINVOICED' and br.processType='FULL_AUTOMATIC') and br.disabled = false order by br.id asc"),
        @NamedQuery(name = "BillingRun.getForInvoicingLimitToIds", query = "SELECT br FROM BillingRun br where (br.status in ('NEW', 'PREVALIDATED', 'POSTVALIDATED') or (br.status='POSTINVOICED' and br.processType='FULL_AUTOMATIC')) and br.id in :ids and br.disabled = false order by br.id asc"),
        @NamedQuery(name = "BillingRun.findByIdAndBCCode", query = "from BillingRun br join fetch br.billingCycle bc where lower(concat(br.id,'/',bc.code)) like :code "),
        @NamedQuery(name = "BillingRun.nullifyBillingRunXMLExecutionResultIds", query = "update BillingRun br set br.xmlJobExecutionResultId = null where br = :billingRun"),
        @NamedQuery(name = "BillingRun.nullifyBillingRunPDFExecutionResultIds", query = "update BillingRun br set br.pdfJobExecutionResultId = null where br = :billingRun") ,
        @NamedQuery(name = "BillingRun.calculateBRStatisticsByInvoices", 
		query = "SELECT SUM(i.amountWithTax) AS amountWithTax, SUM(i.amountTax) AS amountTax, SUM(i.amountWithoutTax) AS amountWithoutTax, COUNT(DISTINCT i.billingAccount.id) AS countBA, COUNT(i) AS countInvoices "
				+ "FROM Invoice i WHERE i.billingRun.id = :billingRunId AND i.status <> 'CANCELED'"),
        @NamedQuery(name = "BillingRun.findByBillingCycle", query = "FROM BillingRun br WHERE br.billingCycle = :bc")})
public class BillingRun extends EnableEntity implements ICustomFieldEntity, IReferenceEntity {

    private static final long serialVersionUID = 1L;

    /**
     * Billing run processing start date
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "process_date")
    private Date processDate;

    /**
     * Execution status
     */
    @Enumerated(value = EnumType.STRING)
    @Column(name = "status")
    private BillingRunStatusEnum status;

    /**
     * Last status change date
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "status_date")
    private Date statusDate;

    /**
     * Billing cycle
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "billing_cycle_id")
    private BillingCycle billingCycle;

    /**
     * Number of matched Billing accounts
     */
    @Column(name = "nb_billing_account")
    private Integer billingAccountNumber;

    /**
     * Number of billable Billing accounts
     */
    @Column(name = "nb_billable_billing_account")
    private Integer billableBillingAcountNumber;

    /**
     * Deprecated in 5.3 for not use
     */
    @Deprecated
    @Column(name = "nb_producible_invoice")
    private Integer producibleInvoiceNumber;

    /**
     * Deprecated in 5.3 for not use
     */
    @Deprecated
    @Column(name = "producible_amount_without_tax", precision = NB_PRECISION, scale = NB_DECIMALS)
    private BigDecimal producibleAmountWithoutTax;

    /**
     * Deprecated in 5.3 for not use
     */
    @Deprecated
    @Column(name = "producible_amount_tax", precision = NB_PRECISION, scale = NB_DECIMALS)
    private BigDecimal producibleAmountTax;

    /**
     * Number of generated invoice
     */
    @Column(name = "nb_invoice")
    private Integer invoiceNumber;

    /**
     * Deprecated in 5.3 for not use
     */
    @Deprecated
    @Column(name = "producible_amount_with_tax", precision = NB_PRECISION, scale = NB_DECIMALS)
    private BigDecimal producibleAmountWithTax;

    /**
     * Billed amount without tax
     */
    @Column(name = "pr_amount_without_tax", precision = NB_PRECISION, scale = NB_DECIMALS)
    private BigDecimal prAmountWithoutTax;

    /**
     * Billed amount with tax
     */
    @Column(name = "pr_amount_with_tax", precision = NB_PRECISION, scale = NB_DECIMALS)
    private BigDecimal prAmountWithTax;

    /**
     * Billed tax amount
     */
    @Column(name = "pr_amount_tax", precision = NB_PRECISION, scale = NB_DECIMALS)
    private BigDecimal prAmountTax;

    /**
     * Invoices produced by a Billing run
     */
    @OneToMany(mappedBy = "billingRun", fetch = FetchType.LAZY)
    private List<Invoice> invoices = new ArrayList<>();

    /**
     * Billing run lists
     */
    @OneToMany(mappedBy = "billingRun", fetch = FetchType.LAZY)
    private Set<BillingRunList> billingRunLists = new HashSet<>();

    /**
     * Billed billing accounts
     */
    @OneToMany(mappedBy = "billingRun", fetch = FetchType.LAZY)
    private List<BillingAccount> billableBillingAccounts = new ArrayList<>();

    /**
     * Billing run processing type
     */
    @Enumerated(value = EnumType.STRING)
    @Column(name = "process_type")
    private BillingProcessTypesEnum processType;

    /**
     * Include Rated transactions from Billing Accounts which next invoicing date is between the dates - from date
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "start_date")
    private Date startDate;

    /**
     * Include Rated transactions from Billing Accounts which next invoicing date is between the dates - to date
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "end_date")
    private Date endDate;

    /**
     * Invoice date
     */
    @Column(name = "invoice_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date invoiceDate;

    /**
     * Include in invoice Rated transactions up to that date
     */
    @Column(name = "last_transaction_date", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastTransactionDate;

    /**
     * Rejection reason
     */
    @Column(name = "rejection_reason")
    @Size(max = 255)
    private String rejectionReason;

    /**
     * Currency
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pr_currency_id")
    private Currency currency;

    /**
     * Country
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pr_country_id")
    private Country country;

    /**
     * Language
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pr_language_id")
    private Language language;

    /**
     * Selected billing accounts (identifiers)
     */
    @Type(type = "longText")
    @Column(name = "selected_billing_accounts")
    private String selectedBillingAccounts;

    /**
     * Rejected billing accounts
     */
    @OneToMany(mappedBy = "billingRun", fetch = FetchType.LAZY)
    private List<RejectedBillingAccount> rejectedBillingAccounts = new ArrayList<>();

    /**
     * Custom field values in JSON format
     */
    @Type(type = "cfjson")
    @Column(name = "cf_values", columnDefinition = "jsonb")
    private CustomFieldValues cfValues;

    /**
     * Accumulated custom field values in JSON format
     */
//    @Type(type = "cfjson")
//    @Column(name = "cf_values_accum", columnDefinition = "TEXT")
    @Transient
    private CustomFieldValues cfAccumulatedValues;

    /**
     * Unique identifier - UUID
     */
    @Column(name = "uuid", nullable = false, updatable = false, length = 60)
    @Size(max = 60)
    @NotNull
    private String uuid;

    /**
     * Reference date
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "reference_date")
    private ReferenceDateEnum referenceDate = ReferenceDateEnum.TODAY;

    @Type(type = "numeric_boolean")
    @Column(name = "skip_validation_script")
    private boolean skipValidationScript = false;

    /**
     * EL to compute invoice.initialCollectionDate delay.
     */
    @Column(name = "collection_date")
    private Date collectionDate;

    /**
     * To decide whether or not dates should be recomputed at invoice validation.
     */
    @Type(type = "numeric_boolean")
    @Column(name = "compute_dates_validation")
    private Boolean computeDatesAtValidation;

    /**
     * The next BillingRun where rejected/suspect invoices may be moved.
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "next_billing_run_id")
    private BillingRun nextBillingRun;

    /**
     * To indicates that invoice minimum job has already been run on the BR.
     */
    @Type(type = "numeric_boolean")
    @Column(name = "minimum_applied")
    private Boolean minimumApplied;

    /**
     * To indicates that invoicing threshold job has already been run on the BR.
     */
    @Type(type = "numeric_boolean")
    @Column(name = "threshold_checked")
    private Boolean thresholdChecked;

    /**
     * To indicates that that invoice discounts job has already been run on the BR.
     */
    @Type(type = "numeric_boolean")
    @Column(name = "discount_applied")
    private Boolean discountApplied;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "reject_auto_action")
    private BillingRunAutomaticActionEnum rejectAutoAction = BillingRunAutomaticActionEnum.MOVE;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "suspect_auto_action")
    private BillingRunAutomaticActionEnum suspectAutoAction = BillingRunAutomaticActionEnum.MOVE;

    /**
     * Filtering option used in exceptional billing run.
     */
    @Type(type = "json")
    @Column(name = "filters", columnDefinition = "jsonb")
    private Map<String, Object> filters;

    @Transient
    private List<Long> exceptionalRTIds;

    @Transient
    private List<Long> exceptionalILIds;

    /**
     * Invoice type
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "invoice_type_id")
    private InvoiceType invoiceType;

    /**
     * Billing run type
     */
    @Enumerated(value = EnumType.STRING)
    @Column(name = "run_type")
    private BillingRunTypeEnum runType;

    @Column(name = "xml_job_execution_result_id")
    private Long xmlJobExecutionResultId;

    @Column(name = "pdf_job_execution_result_id")
    private Long pdfJobExecutionResultId;

    /**
     * i18n Description
     */
    @Type(type = "json")
    @Column(name = "description_i18n", columnDefinition = "jsonb")
    private Map<String, String> descriptionI18n;

    @Type(type = "numeric_boolean")
    @Column(name = "is_quarantine")
    private Boolean isQuarantine;

    /**
     * The origin BillingRun from where draft/rejected/suspect invoice is from.
     */
    @OneToOne
    @JoinColumn(name = "origin_billing_run_id")
    private BillingRun originBillingRun;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "billing_run_job_execution", joinColumns = @JoinColumn(name = "billing_run_id"), inverseJoinColumns = @JoinColumn(name = "job_execution_id"))
    protected List<JobExecutionResultImpl> jobExecutions = new ArrayList<>();

    /**
     * To decide whether or not generate account oeprations.
     */
    @Type(type = "numeric_boolean")
    @Column(name = "generate_ao", nullable = false)
    private Boolean generateAO = Boolean.FALSE;

    /**
     * Do not aggregate RTs to ILs at all
     */
    @Type(type = "numeric_boolean")
    @Column(name = "disable_aggregation")
    private boolean disableAggregation = false;

    /**
     * To decide if adding invoice lines incrementally or not.
     */
    @Type(type = "numeric_boolean")
    @Column(name = "incremental_invoice_lines")
    private Boolean incrementalInvoiceLines = Boolean.FALSE;

    /**
     * /** Aggregate by date option
     */
    @Enumerated(value = EnumType.STRING)
    @Column(name = "date_aggregation")
    private DateAggregationOption dateAggregation = DateAggregationOption.MONTH_OF_USAGE_DATE;

    /**
     * Aggregate per unit amount
     */
    @Type(type = "numeric_boolean")
    @Column(name = "aggregate_unit_amounts")
    private boolean aggregateUnitAmounts = false;

    /**
     * Aggregate based on accounting article label instead of RT description
     */
    @Type(type = "numeric_boolean")
    @Column(name = "use_accounting_article_label")
    private boolean useAccountingArticleLabel = false;

    /**
     * If TRUE, aggregation will ignore subscription field (multiple subscriptions will be aggregated together)
     */
    @Type(type = "numeric_boolean")
    @Column(name = "ignore_subscriptions")
    private boolean ignoreSubscriptions = true;

    /**
     * If TRUE, aggregation will ignore order field (multiple orders will be aggregated together)
     */
    @Type(type = "numeric_boolean")
    @Column(name = "ignore_orders")
    private boolean ignoreOrders = true;

    /**
     * Discount aggregation type
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "discount_aggregation", nullable = false)
    private DiscountAggregationModeEnum discountAggregation = DiscountAggregationModeEnum.FULL_AGGREGATION;

    /**
     * To decide if billing run report will be generated during billing run creation
     */
    @Type(type = "numeric_boolean")
    @Column(name = "pre_report_auto_on_create")
    private boolean preReportAutoOnCreate = false;

    /**
     * Billing run report generate during billing run creation.
     */
    @OneToOne(fetch = LAZY)
    @JoinColumn(name = "pre_invoicing_report_id")
    private BillingRunReport preInvoicingReport;

    /**
     * To decide if billing run report will be generated during invoice line job
     */
    @Type(type = "numeric_boolean")
    @Column(name = "pre_report_auto_on_invoice_line_job")
    private boolean preReportAutoOnInvoiceLinesJob = false;

    /**
     * An expression to decide if a billing run will be processed or ignored by the jobs.
     */
    @Column(name = "application_el", length = 2000)
    @Size(max = 2000)
    private String applicationEl;

    /**
     * Billing run aggregation setting for user accounts
     */
    @Type(type = "numeric_boolean")
    @Column(name = "ignore_user_accounts")
    private boolean ignoreUserAccounts = true;

    /**
     * Define additional criterias for aggregation
     */
    @ElementCollection(fetch = FetchType.LAZY)
    @CollectionTable(name = "billing_run_aggregation_fields", joinColumns = @JoinColumn(name = "billing_run_id"))
    @Column(name = "fields")
    private List<String> additionalAggregationFields;

    public BillingRun getNextBillingRun() {
        return nextBillingRun;
    }

    public void setNextBillingRun(BillingRun nextBillingRun) {
        this.nextBillingRun = nextBillingRun;
    }

    public Date getProcessDate() {
        return processDate;
    }

    public void setProcessDate(Date processDate) {
        this.processDate = processDate;
    }

    public BillingRunStatusEnum getStatus() {
        return status;
    }

    public void setStatus(BillingRunStatusEnum status) {
        this.status = status;
    }

    public Date getStatusDate() {
        return statusDate;
    }

    public void setStatusDate(Date statusDate) {
        this.statusDate = statusDate;
    }

    public BillingCycle getBillingCycle() {
        return billingCycle;
    }

    public void setBillingCycle(BillingCycle billingCycle) {
        this.billingCycle = billingCycle;
    }

    public Integer getBillingAccountNumber() {
        return billingAccountNumber;
    }

    public void setBillingAccountNumber(Integer billingAccountNumber) {
        this.billingAccountNumber = billingAccountNumber;
    }

    public Integer getBillableBillingAcountNumber() {
        return billableBillingAcountNumber;
    }

    public void setBillableBillingAcountNumber(Integer billableBillingAcountNumber) {
        this.billableBillingAcountNumber = billableBillingAcountNumber;
    }

    public Integer getProducibleInvoiceNumber() {
        return producibleInvoiceNumber;
    }

    public void setProducibleInvoiceNumber(Integer producibleInvoiceNumber) {
        this.producibleInvoiceNumber = producibleInvoiceNumber;
    }

    public BigDecimal getProducibleAmountWithoutTax() {
        return producibleAmountWithoutTax;
    }

    public void setProducibleAmountWithoutTax(BigDecimal producibleAmountWithoutTax) {
        this.producibleAmountWithoutTax = producibleAmountWithoutTax;
    }

    public BigDecimal getProducibleAmountTax() {
        return producibleAmountTax;
    }

    public void setProducibleAmountTax(BigDecimal producibleAmountTax) {
        this.producibleAmountTax = producibleAmountTax;
    }

    public Integer getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(Integer invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public BigDecimal getProducibleAmountWithTax() {
        return producibleAmountWithTax;
    }

    public void setProducibleAmountWithTax(BigDecimal producibleAmountWithTax) {
        this.producibleAmountWithTax = producibleAmountWithTax;
    }

    public void setPrAmountTax(BigDecimal prAmountTax) {
        this.prAmountTax = prAmountTax;
    }

    public List<Invoice> getInvoices() {
        return invoices;
    }

    public void setInvoices(List<Invoice> invoices) {
        this.invoices = invoices;
    }

    public Set<BillingRunList> getBillingRunLists() {
        return billingRunLists;
    }

    public void setBillingRunLists(Set<BillingRunList> billingRunLists) {
        this.billingRunLists = billingRunLists;
    }

    public BillingProcessTypesEnum getProcessType() {
        return processType;
    }

    public void setProcessType(BillingProcessTypesEnum processType) {
        this.processType = processType;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(Date invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    /**
     * @return Include in invoice Rated transactions up to that date
     */
    public Date getLastTransactionDate() {
        return lastTransactionDate;
    }

    /**
     * @param lastTransactionDate Include in invoice Rated transactions up to that date
     */
    public void setLastTransactionDate(Date lastTransactionDate) {
        this.lastTransactionDate = lastTransactionDate;
    }

    public List<BillingAccount> getBillableBillingAccounts() {
        return billableBillingAccounts;
    }

    public void setBillableBillingAccounts(List<BillingAccount> selectedBillingAccounts) {
        this.billableBillingAccounts = selectedBillingAccounts;
    }

    public String getRejectionReason() {
        return rejectionReason;
    }

    public void setRejectionReason(String rejectionReason) {
        this.rejectionReason = rejectionReason;
    }

    public BigDecimal getPrAmountWithoutTax() {
        return prAmountWithoutTax;
    }

    public void setPrAmountWithoutTax(BigDecimal prAmountWithoutTax) {
        this.prAmountWithoutTax = prAmountWithoutTax;
    }

    public BigDecimal getPrAmountWithTax() {
        return prAmountWithTax;
    }

    public void setPrAmountWithTax(BigDecimal prAmountWithTax) {
        this.prAmountWithTax = prAmountWithTax;
    }

    public BigDecimal getPrAmountTax() {
        return prAmountTax;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public String getSelectedBillingAccounts() {
        return selectedBillingAccounts;
    }

    public void setSelectedBillingAccounts(String selectedBillingAccounts) {
        this.selectedBillingAccounts = selectedBillingAccounts;
    }

    public List<RejectedBillingAccount> getRejectedBillingAccounts() {
        return rejectedBillingAccounts;
    }

    public void setRejectedBillingAccounts(List<RejectedBillingAccount> rejectedBillingAccounts) {
        this.rejectedBillingAccounts = rejectedBillingAccounts;
    }

    public void addRejectedBillingAccounts(RejectedBillingAccount rejectedBillingAccount) {
        if (rejectedBillingAccounts == null) {
            rejectedBillingAccounts = new ArrayList<RejectedBillingAccount>();
        }
        rejectedBillingAccounts.add(rejectedBillingAccount);
    }

    @Override
    public CustomFieldValues getCfValues() {
        return cfValues;
    }

    @Override
    public void setCfValues(CustomFieldValues cfValues) {
        this.cfValues = cfValues;
    }

    /**
     * setting uuid if null
     */
    @PrePersist
    @PreUpdate
    public void setUUIDIfNull() {
        if (uuid == null) {
            uuid = UUID.randomUUID().toString();
        }
    }

    @Override
    public CustomFieldValues getCfAccumulatedValues() {
        return cfAccumulatedValues;
    }

    @Override
    public void setCfAccumulatedValues(CustomFieldValues cfAccumulatedValues) {
        this.cfAccumulatedValues = cfAccumulatedValues;
    }

    @Override
    public String getUuid() {
        setUUIDIfNull(); // setting uuid if null to be sure that the existing code expecting uuid not null will not be impacted
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Override
    public ICustomFieldEntity[] getParentCFEntities() {
        return null;
    }

    @Override
    public String clearUuid() {
        String oldUuid = uuid;
        uuid = UUID.randomUUID().toString();
        return oldUuid;
    }

    @Override
    public int hashCode() {
        return 961 + (("BR" + (id == null ? "" : id)).hashCode());
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj == null) {
            return false;
        } else if (!(obj instanceof User)) {
            return false;
        }

        BillingRun other = (BillingRun) obj;

        if (getId() != null && other.getId() != null && getId().equals(other.getId())) {
            return true;
        }

        if (id == null) {
            if (other.getId() != null) {
                return false;
            }
        } else if (!id.equals(other.getId())) {
            return false;
        }
        return true;
    }

    public void setDescriptionOrCode(String description) {

    }

    public String getReferenceCode() {
        final String bcCode = billingCycle == null ? "" : "/" + billingCycle.getCode();
        return id + bcCode;
    }

    public void setReferenceCode(Object value) {
        String id = null;
        if (value != null) {
            id = value.toString().split("/")[0];
            setId(Long.valueOf(id));
            billingCycle = new BillingCycle();
            billingCycle.setCode(getBillingCycleCode(value.toString(), id));
        }
    }

    private String getBillingCycleCode(String value, String id) {
        return value.substring(id.length() + 1, value.length());
    }

    public String getReferenceDescription() {
        return billingCycle.getDescription();
    }

    /**
     * Gets the reference date
     *
     * @return the reference date
     */
    public ReferenceDateEnum getReferenceDate() {
        return referenceDate;
    }

    /**
     * Sets the reference date.
     *
     * @param referenceDate the new reference date
     */
    public void setReferenceDate(ReferenceDateEnum referenceDate) {
        this.referenceDate = referenceDate;
    }

    public BillingRunAutomaticActionEnum getRejectAutoAction() {
        return rejectAutoAction;
    }

    public void setRejectAutoAction(BillingRunAutomaticActionEnum autoRejectAction) {
        this.rejectAutoAction = autoRejectAction;
    }

    public BillingRunAutomaticActionEnum getSuspectAutoAction() {
        return suspectAutoAction;
    }

    public void setSuspectAutoAction(BillingRunAutomaticActionEnum autoSuspectAction) {
        this.suspectAutoAction = autoSuspectAction;
    }

    public boolean isSkipValidationScript() {
        return skipValidationScript;
    }

    public void setSkipValidationScript(boolean skipValidationScript) {
        this.skipValidationScript = skipValidationScript;
    }

    public Date getCollectionDate() {
        return collectionDate;
    }

    public void setCollectionDate(Date collectionDate) {
        this.collectionDate = collectionDate;
    }

    public Boolean getComputeDatesAtValidation() {
        return computeDatesAtValidation;
    }

    public void setComputeDatesAtValidation(Boolean computeDatesAtValidation) {
        this.computeDatesAtValidation = computeDatesAtValidation;
    }

    /**
     * @return the minimumApplied
     */
    public Boolean getMinimumApplied() {
        return minimumApplied;
    }

    /**
     * @param minimumApplied the minimumApplied to set
     */
    public void setMinimumApplied(Boolean minimumApplied) {
        this.minimumApplied = minimumApplied;
    }

    /**
     * @return the thresholdChecked
     */
    public Boolean getThresholdChecked() {
        return thresholdChecked;
    }

    /**
     * @param thresholdChecked the thresholdChecked to set
     */
    public void setThresholdChecked(Boolean thresholdChecked) {
        this.thresholdChecked = thresholdChecked;
    }

    /**
     * @return the discountApplied
     */
    public Boolean getDiscountApplied() {
        return discountApplied;
    }

    /**
     * @param discountApplied the discountApplied to set
     */
    public void setDiscountApplied(Boolean discountApplied) {
        this.discountApplied = discountApplied;
    }

    public Map<String, Object> getFilters() {
        return filters;
    }

    public void setFilters(Map<String, Object> filters) {
        this.filters = filters;
    }

    public List<Long> getExceptionalRTIds() {
        return exceptionalRTIds;
    }

    public void setExceptionalRTIds(List<Long> exceptionalRTIds) {
        this.exceptionalRTIds = exceptionalRTIds;
    }

    public boolean isExceptionalBR() {
        return (this.filters != null && !this.filters.isEmpty());
    }

    public List<Long> getExceptionalILIds() {
        return exceptionalILIds;
    }

    public void setExceptionalILIds(List<Long> exceptionalILIds) {
        this.exceptionalILIds = exceptionalILIds;
    }

    public InvoiceType getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(InvoiceType invoiceType) {
        this.invoiceType = invoiceType;
    }

    public BillingRunTypeEnum getRunType() {
        return runType;
    }

    public void setRunType(BillingRunTypeEnum runType) {
        this.runType = runType;
    }

    public Long getXmlJobExecutionResultId() {
        return xmlJobExecutionResultId;
    }

    public void setXmlJobExecutionResultId(Long xmlJobExecutionResultId) {
        this.xmlJobExecutionResultId = xmlJobExecutionResultId;
    }

    public Long getPdfJobExecutionResultId() {
        return pdfJobExecutionResultId;
    }

    public void setPdfJobExecutionResultId(Long pdfJobExecutionResultId) {
        this.pdfJobExecutionResultId = pdfJobExecutionResultId;
    }

    public Map<String, String> getDescriptionI18n() {
        return descriptionI18n;
    }

    public void setDescriptionI18n(Map<String, String> descriptionI18n) {
        this.descriptionI18n = descriptionI18n;
    }

    public Boolean getIsQuarantine() {
        return isQuarantine;
    }

    public void setIsQuarantine(Boolean isQuarantine) {
        this.isQuarantine = isQuarantine;
    }

    public BillingRun getOriginBillingRun() {
        return originBillingRun;
    }

    public void setOriginBillingRun(BillingRun originBillingRun) {
        this.originBillingRun = originBillingRun;
    }

    public List<JobExecutionResultImpl> getJobExecutions() {
        return jobExecutions;
    }

    public void setJobExecutions(List<JobExecutionResultImpl> jobExecutions) {
        this.jobExecutions = jobExecutions;
    }

    public void addJobExecutions(JobExecutionResultImpl jobExecution) {
        this.jobExecutions.add(jobExecution);
    }

    public Boolean getGenerateAO() {
        return generateAO;
    }

    public void setGenerateAO(Boolean generateAO) {
        this.generateAO = generateAO;
    }

    public boolean isDisableAggregation() {
        return disableAggregation;
    }

    public void setDisableAggregation(boolean disableAggregation) {
        this.disableAggregation = disableAggregation;
    }

    /**
     * @return To decide if adding invoice lines incrementally or not.
     */
    public Boolean getIncrementalInvoiceLines() {
        return incrementalInvoiceLines;
    }

    /**
     * @param incrementalInvoiceLines To decide if adding invoice lines incrementally or not.
     */
    public void setIncrementalInvoiceLines(Boolean incrementalInvoiceLines) {
        this.incrementalInvoiceLines = incrementalInvoiceLines;
    }

    /**
     * @return Aggregate by date option
     */
    public DateAggregationOption getDateAggregation() {
        return dateAggregation;
    }

    /**
     * @param dateAggregation Aggregate by date option
     */
    public void setDateAggregation(DateAggregationOption dateAggregation) {
        this.dateAggregation = dateAggregation;
    }

    /**
     * @return Aggregate per unit amount
     */
    public boolean isAggregateUnitAmounts() {
        return aggregateUnitAmounts;
    }

    /**
     * @param aggregateUnitAmounts Aggregate per unit amount
     */
    public void setAggregateUnitAmounts(boolean aggregateUnitAmounts) {
        this.aggregateUnitAmounts = aggregateUnitAmounts;
    }

    /**
     * @return Aggregate based on accounting article label instead of RT description
     */
    public boolean isUseAccountingArticleLabel() {
        return useAccountingArticleLabel;
    }

    /**
     * @param useAccountingArticleLabel Aggregate based on accounting article label instead of RT description
     */
    public void setUseAccountingArticleLabel(boolean useAccountingArticleLabel) {
        this.useAccountingArticleLabel = useAccountingArticleLabel;
    }

    /**
     * @return If TRUE, aggregation will ignore subscription field (multiple subscriptions will be aggregated together)
     */
    public boolean isIgnoreSubscriptions() {
        return ignoreSubscriptions;
    }

    /**
     * @param ignoreSubscriptions If TRUE, aggregation will ignore subscription field (multiple subscriptions will be aggregated together)
     */
    public void setIgnoreSubscriptions(boolean ignoreSubscriptions) {
        this.ignoreSubscriptions = ignoreSubscriptions;
    }

    /**
     * @return If TRUE, aggregation will ignore order field (multiple orders will be aggregated together)
     */
    public boolean isIgnoreOrders() {
        return ignoreOrders;
    }

    /**
     * @param ignoreOrders If TRUE, aggregation will ignore order field (multiple orders will be aggregated together)
     */
    public void setIgnoreOrders(boolean ignoreOrders) {
        this.ignoreOrders = ignoreOrders;
    }

    /**
     * @return Discount aggregation type
     */
    public DiscountAggregationModeEnum getDiscountAggregation() {
        return discountAggregation;
    }

    /**
     * @param discountAggregation Discount aggregation type
     */
    public void setDiscountAggregation(DiscountAggregationModeEnum discountAggregation) {
        this.discountAggregation = discountAggregation;
    }

    public boolean isPreReportAutoOnCreate() {
        return preReportAutoOnCreate;
    }

    public void setPreReportAutoOnCreate(boolean preReportAutoOnCreate) {
        this.preReportAutoOnCreate = preReportAutoOnCreate;
    }

    public BillingRunReport getPreInvoicingReport() {
        return preInvoicingReport;
    }

    public void setPreInvoicingReport(BillingRunReport preInvoicingReport) {
        this.preInvoicingReport = preInvoicingReport;
    }

    public boolean hasPreInvoicingReport() {
        return preInvoicingReport != null;
    }

    public boolean isPreReportAutoOnInvoiceLinesJob() {
        return preReportAutoOnInvoiceLinesJob;
    }

    public void setPreReportAutoOnInvoiceLinesJob(boolean preReportAutoOnInvoiceLinesJob) {
        this.preReportAutoOnInvoiceLinesJob = preReportAutoOnInvoiceLinesJob;
    }

    public String getApplicationEl() {
        return applicationEl;
    }

    public void setApplicationEl(String applicationEl) {
        this.applicationEl = applicationEl;
    }

	public boolean isIgnoreUserAccounts() {
		return ignoreUserAccounts;
}

	public void setIgnoreUserAccounts(boolean ignoreUserAccounts) {
		this.ignoreUserAccounts = ignoreUserAccounts;
	}

    public List<String> getAdditionalAggregationFields() {
        return additionalAggregationFields;
    }

    public void setAdditionalAggregationFields(List<String> additionalAggregationFields) {
        this.additionalAggregationFields = additionalAggregationFields;
    }
    
    public void addInvoiceNumber(int count) {
    	this.invoiceNumber=(this.invoiceNumber!=null?this.invoiceNumber:0)+count;
    }

}
