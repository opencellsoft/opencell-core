package org.meveo.model.billing;

public enum EvaluationModeEnum {

    VALIDATION, REJECTION, CONDITION;
}
