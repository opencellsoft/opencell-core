package org.meveo.model.billing;

public enum RatedTransactionAction {
    REOPEN, CANCEL
}
