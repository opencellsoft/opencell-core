package org.meveo.model.billing;

public enum InvoiceTypeEnum {
    ADJUSTMENT, ADVANCEMENT_PAYMENT
}
