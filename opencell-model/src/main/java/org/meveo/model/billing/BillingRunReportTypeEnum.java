package org.meveo.model.billing;

public enum BillingRunReportTypeEnum {

    OPEN_RATED_TRANSACTIONS,
    BILLED_RATED_TRANSACTIONS,
    OPEN_INVOICE_LINES,
    BILLED_INVOICE_LINES
}
