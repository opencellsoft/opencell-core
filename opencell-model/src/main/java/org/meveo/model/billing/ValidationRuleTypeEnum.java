package org.meveo.model.billing;

/**
 *
 */
public enum ValidationRuleTypeEnum {
	SCRIPT, EXPRESSION_LANGUAGE, RULE_SET;
}
