package org.meveo.model.admin.partitioning;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "tech_wo_partition_log")
public class WoPartitionLog extends AbstractPartitionLog {
}
