package org.meveo.model.settings;

public enum MaximumValidityUnitEnum {
    Days, Weeks, Months, Years;
}
