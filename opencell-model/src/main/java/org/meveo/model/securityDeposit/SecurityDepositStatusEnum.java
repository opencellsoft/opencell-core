package org.meveo.model.securityDeposit;

public enum SecurityDepositStatusEnum {
    DRAFT,
    
    LOCKED,
    
    UNLOCKED,
    
    AUTO_REFUND,
    
    REFUNDED,
    
    CANCELED,
    
    HOLD,
    
    VALIDATED
}
