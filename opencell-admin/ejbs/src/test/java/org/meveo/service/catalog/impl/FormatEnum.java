package org.meveo.service.catalog.impl;

public enum FormatEnum {
    CSV("csv");

    private final String label;

    FormatEnum(String label) {
        this.label = label;
    }
}
