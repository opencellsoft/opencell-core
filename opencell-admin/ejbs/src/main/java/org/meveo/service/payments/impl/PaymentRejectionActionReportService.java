package org.meveo.service.payments.impl;

import javax.ejb.Stateless;

import org.meveo.model.payments.PaymentRejectionActionReport;
import org.meveo.service.base.BusinessService;

@Stateless
public class PaymentRejectionActionReportService extends BusinessService<PaymentRejectionActionReport> {
}
