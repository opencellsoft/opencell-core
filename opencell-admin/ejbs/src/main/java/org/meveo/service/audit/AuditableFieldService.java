/*
 * (C) Copyright 2015-2020 Opencell SAS (https://opencellsoft.com/) and contributors.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW. EXCEPT WHEN
 * OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS
 * IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE ENTIRE RISK AS TO
 * THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU. SHOULD THE PROGRAM PROVE DEFECTIVE,
 * YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.
 *
 * For more information on the GNU Affero General Public License, please consult
 * <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

package org.meveo.service.audit;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;

import org.meveo.admin.util.pagination.PaginationConfiguration;
import org.meveo.commons.utils.ReflectionUtils;
import org.meveo.model.AuditableField;
import org.meveo.model.BaseEntity;
import org.meveo.service.base.PersistenceService;

/**
 * The Fields audit service.
 *
 * @author Abdellatif BARI
 * @since 7.0
 */

@Stateless
public class AuditableFieldService extends PersistenceService<AuditableField> {

    /**
     * Get auditable fields list
     *
     * @param entity auditable fields entity
     * @return auditable fields list
     */
    public List<AuditableField> list(BaseEntity entity) {
        Map<String, Object> filters = new HashMap<>();
        filters.put("entityClass", ReflectionUtils.getCleanClassName(entity.getClass().getName()));
        filters.put("entityId", entity.getId());
        PaginationConfiguration config = new PaginationConfiguration(filters);
        return list(config);
    }

    /**
     * Purge audit field changes history older than purge date.
     *
     * @param purgeDate the purge date
     * @return Number of records deleted
     */
    public int purgeAuditableField(Date purgeDate) {
        return getEntityManager().createNamedQuery("AuditableField.purgeAuditableField").setParameter("purgeDate", purgeDate).executeUpdate();
    }
}