package org.meveo.service.billing.impl;

import org.meveo.model.billing.ProductAmount;
import org.meveo.service.base.PersistenceService;

import javax.ejb.Stateless;

@Stateless
public class ProductAmountService extends PersistenceService<ProductAmount> {
}
