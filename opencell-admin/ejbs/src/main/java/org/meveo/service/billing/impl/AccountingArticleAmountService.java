package org.meveo.service.billing.impl;

import org.meveo.model.billing.AccountingArticleAmount;
import org.meveo.service.base.PersistenceService;

import javax.ejb.Stateless;

@Stateless
public class AccountingArticleAmountService extends PersistenceService<AccountingArticleAmount> {
}
