package org.meveo.service.tech;

import org.meveo.model.admin.partitioning.RTPartitionLog;
import org.meveo.service.base.PersistenceService;

import javax.ejb.Stateless;

@Stateless
public class RtPartitionLogService extends PersistenceService<RTPartitionLog> {
}
