package org.meveo.service.accountingscheme;

import org.meveo.model.accountingScheme.AccountingCodeMapping;
import org.meveo.service.base.PersistenceService;

import javax.ejb.Stateless;

@Stateless
public class AccountingCodeMappingService extends PersistenceService<AccountingCodeMapping> {
}