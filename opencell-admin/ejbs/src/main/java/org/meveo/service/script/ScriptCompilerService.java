/*
 * (C) Copyright 2015-2020 Opencell SAS (https://opencellsoft.com/) and contributors.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW. EXCEPT WHEN
 * OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS
 * IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE ENTIRE RISK AS TO
 * THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU. SHOULD THE PROGRAM PROVE DEFECTIVE,
 * YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.
 *
 * For more information on the GNU Affero General Public License, please consult
 * <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */
package org.meveo.service.script;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.naming.InitialContext;
import javax.tools.Diagnostic;
import javax.tools.DiagnosticCollector;
import javax.tools.JavaFileObject;

import org.apache.commons.lang3.SystemUtils;
import org.infinispan.Cache;
import org.infinispan.context.Flag;
import org.meveo.admin.exception.ElementNotFoundException;
import org.meveo.admin.exception.InvalidScriptException;
import org.meveo.cache.CacheKeyStr;
import org.meveo.commons.utils.EjbUtils;
import org.meveo.commons.utils.FileUtils;
import org.meveo.commons.utils.MethodCallingUtils;
import org.meveo.commons.utils.ParamBean;
import org.meveo.interceptor.ConcurrencyLock;
import org.meveo.jpa.JpaAmpNewTx;
import org.meveo.model.scripts.ScriptInstance;
import org.meveo.model.scripts.ScriptInstanceError;
import org.meveo.model.scripts.ScriptSourceTypeEnum;
import org.meveo.service.base.BusinessService;
import org.slf4j.Logger;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

/**
 * Compiles scripts and provides compiled script classes.
 * 
 * NOTE: Compilation methods are executed synchronously due to WRITE lock. DO NOT CHANGE IT, so there would be only one attempt to compile a new script class
 * 
 * @author Andrius Karpavicius
 * @lastModifiedVersion 7.2.0
 *
 */
@Stateless
public class ScriptCompilerService extends BusinessService<ScriptInstance> implements Serializable {

    private static final long serialVersionUID = -2361674789184033495L;

    /**
     * Stores compiled scripts. Key format: &lt;cluster node code&gt;_&lt;scriptInstance code&gt;. Value is a compiled script class and class instance
     */
    @Resource(lookup = "java:jboss/infinispan/cache/opencell/opencell-script-cache")
    private Cache<CacheKeyStr, Class<ScriptInterface>> compiledScripts;

    @Inject
    private MethodCallingUtils methodCallingUtils;

    /**
     * A constructed class path across all script compilations
     */
    private static String classpath = "";

    /**
     * Just a variable to synchronize calls to classpath generation
     */
    private static String varForSync = "sync";

    /**
     * Get all script interfaces with compiling those that are not compiled yet
     * 
     * @return the allScriptInterfaces
     */
    public List<Class<ScriptInterface>> getAllScriptInterfacesWCompile() {

        List<Class<ScriptInterface>> scriptInterfaces = new ArrayList<>();

        List<ScriptInstance> scriptInstances = findByType(ScriptSourceTypeEnum.JAVA);
        for (ScriptInstance scriptInstance : scriptInstances) {
            if (!scriptInstance.isError()) {
                try {
                    scriptInterfaces.add(getOrCompileScript(scriptInstance.getCode()));
                } catch (ElementNotFoundException | InvalidScriptException e) {
                    // Ignore errors here as they were logged in a call before
                }
            }
        }

        return scriptInterfaces;
    }

    /**
     * Find scripts by source type.
     * 
     * @param type script source type
     * @return list of scripts
     */
    @SuppressWarnings("unchecked")
    public List<ScriptInstance> findByType(ScriptSourceTypeEnum type) {
        List<ScriptInstance> result = new ArrayList<ScriptInstance>();
        try {
            result = (List<ScriptInstance>) getEntityManager().createNamedQuery("CustomScript.getScriptInstanceByTypeActive").setParameter("sourceTypeEnum", type).getResultList();
        } catch (NoResultException e) {

        }
        return result;
    }

    /**
     * Construct classpath for script compilation
     * 
     * @throws IOException Failed to access library files
     * @throws URISyntaxException Failed to access library files
     */
    private void constructClassPath() throws IOException, URISyntaxException {

        synchronized (varForSync) {
            if (classpath.length() == 0) {

                // Check if deploying an exploded archive or a compressed file
                String thisClassfile = this.getClass().getProtectionDomain().getCodeSource().getLocation().getFile();

                File realFile = new File(thisClassfile);

                // Was deployed as exploded archive
                if (realFile.exists()) {
                    File deploymentDir = realFile.getParentFile();
                    for (File file : deploymentDir.listFiles()) {
                        if (file.getName().endsWith(".jar")) {
                            classpath += file.getCanonicalPath() + File.pathSeparator;
                        }
                    }

                    // War was deployed as compressed archive
                } else {

                    org.jboss.vfs.VirtualFile vFile = org.jboss.vfs.VFS.getChild(thisClassfile);
                    realFile = new File(org.jboss.vfs.VFSUtils.getPhysicalURI(vFile).getPath());

                    File deploymentDir = realFile.getParentFile().getParentFile();

                    for (File physicalLibDir : deploymentDir.listFiles()) {
                        if (physicalLibDir.isDirectory()) {
                            for (File f : FileUtils.listFiles(physicalLibDir, "jar", "*", null)) {
                                classpath += f.getCanonicalPath() + File.pathSeparator;
                            }
                        }

                    }
                }

                // Add classes that are usually used in scripts without a need to explicitly import them
                addToClassPath(Logger.class.getName());
                addToClassPath(EntityManager.class.getName());
            }
            log.info("Use classpath for script compilation: {}", classpath);
        }
    }

    /**
     * Compile script. DOES NOT update script entity status. Successfully compiled script will be instantiated and added to a compiled script cache. Optionally Script.init() method is called during script instantiation
     * if requested so.
     * 
     * Script is not cached if disabled or in test compilation mode.
     * 
     * @param scriptCode Script entity code
     * @param sourceType Source code language type
     * @param sourceCode Source code
     * @param isActive Is script active. It will compile it anyway. Will clear but not overwrite existing compiled script cache.
     * @param initialize Should script be initialized when instantiating
     * @param testCompile Is it a compilation for testing purpose. Won't clear nor overwrite existing compiled script cache.
     * 
     * @return A list of compilation errors if not compiled
     */
    @JpaAmpNewTx
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public List<ScriptInstanceError> compileScript(String scriptCode, ScriptSourceTypeEnum sourceType, String sourceCode, boolean isActive, boolean initialize, boolean testCompile) {

        log.debug("Compile script {}", scriptCode);

        try {

            if (!testCompile) {
                clearCompiledScript(scriptCode);
            }

            constructClassPath();

            // For now no need to check source type if (sourceType==ScriptSourceTypeEnum.JAVA){

            Class<ScriptInterface> compiledScript = compileJavaSource(sourceCode);

            if (!testCompile && isActive) {

                CacheKeyStr cacheKey = new CacheKeyStr(currentUser.getProviderCode(), EjbUtils.getCurrentClusterNode() + "_" + scriptCode);

                @SuppressWarnings("deprecation")
                ScriptInterface scriptInstance = compiledScript.newInstance();
                if (initialize) {
                    log.debug("Will initialize script {}", scriptCode);
                    try {
                        scriptInstance.init(null);
                    } catch (Exception e) {
                        log.warn("Failed to initialize script for a cached script instance", e);
                    }
                }

                log.info("Adding compiled script {} into compiled script cache with key {}", scriptCode, cacheKey);

                compiledScripts.put(cacheKey, compiledScript);
                InitialContext ic = new InitialContext();
                ic.rebind("java:global/" + ParamBean.getInstance().getProperty("opencell.moduleName", "opencell") + "/" + scriptCode, scriptInstance);
            }

            return null;

        } catch (CharSequenceCompilerException e) {
            log.error("Failed to compile script {}. ClassPath used: {}. Compilation errors:", scriptCode, classpath);

            List<ScriptInstanceError> scriptErrors = new ArrayList<>();

            List<Diagnostic<? extends JavaFileObject>> diagnosticList = e.getDiagnostics().getDiagnostics();
            for (Diagnostic<? extends JavaFileObject> diagnostic : diagnosticList) {
                if ("ERROR".equals(diagnostic.getKind().name())) {
                    ScriptInstanceError scriptInstanceError = new ScriptInstanceError();
                    scriptInstanceError.setMessage(diagnostic.getMessage(Locale.getDefault()));
                    scriptInstanceError.setLineNumber(diagnostic.getLineNumber());
                    scriptInstanceError.setColumnNumber(diagnostic.getColumnNumber());
                    scriptInstanceError.setSourceFile(diagnostic.getSource()!=null? diagnostic.getSource().toString():null);
                    // scriptInstanceError.setScript(scriptInstance);
                    scriptErrors.add(scriptInstanceError);
                    // scriptInstanceErrorService.create(scriptInstanceError, scriptInstance.getAuditable().getCreator());
                    log.warn("{} script {} location {}:{}: {}", diagnostic.getKind().name(), scriptCode, diagnostic.getLineNumber(), diagnostic.getColumnNumber(), diagnostic.getMessage(Locale.getDefault()));
                }
            }
            return scriptErrors;

        } catch (IOException | URISyntaxException e) {
            log.error("Failed to construct class path for script {}", scriptCode, e);
            return null;

        } catch (Exception e) {
            log.error("Failed while compiling script {}", scriptCode, e);
            List<ScriptInstanceError> scriptErrors = new ArrayList<>();
            ScriptInstanceError scriptInstanceError = new ScriptInstanceError();
            scriptInstanceError.setMessage(e.getMessage() != null ? e.getMessage() : e.getClass().getSimpleName());
            scriptErrors.add(scriptInstanceError);

            return scriptErrors;
        }
    }

    /**
     * Compile java Source script
     * 
     * @param javaSrc Java source to compile
     * @return Compiled class instance
     * @throws CharSequenceCompilerException char sequence compiler exception.
     */
    public Class<ScriptInterface> compileJavaSource(String javaSrc) throws CharSequenceCompilerException {

        supplementClassPathWithMissingImports(javaSrc);

        String fullClassName = ScriptUtils.getFullClassname(javaSrc);

        log.debug("Compile JAVA script {} with classpath {}", fullClassName, classpath);

        CharSequenceCompiler<ScriptInterface> compiler = new CharSequenceCompiler<ScriptInterface>(this.getClass().getClassLoader(), Arrays.asList(new String[] { "-cp", classpath }));
        final DiagnosticCollector<JavaFileObject> errs = new DiagnosticCollector<JavaFileObject>();
        Class<ScriptInterface> compiledScript = compiler.compile(fullClassName, javaSrc, errs, new Class<?>[] { ScriptInterface.class });
        return compiledScript;
    }

    /**
     * Supplement classpath with classes needed for the particular script compilation. Solves issue when classes server as jboss modules are referenced in script. E.g. org.slf4j.Logger
     * 
     * @param javaSrc Java source to compile
     */
    private void supplementClassPathWithMissingImports(String javaSrc) {

        String regex = "import (.*?);";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(javaSrc);
        while (matcher.find()) {
            String className = matcher.group(1);
            if (className.startsWith("static ")) {
                className = className.substring(7, className.lastIndexOf("."));
            }
            addToClassPath(className);
        }
    }

    /**
     * Supplement classpath with a class
     * 
     * @param className Classname
     * @return True if classname was added to the classpath
     */
    public boolean addToClassPath(String className) {

        try {
            if (!className.startsWith("java.") && !className.startsWith("org.meveo")) {

                @SuppressWarnings({ "rawtypes" })
                Class clazz = Class.forName(className);

                String location = clazz.getProtectionDomain().getCodeSource().getLocation().getFile();
                if (location.startsWith("file:")) {
                    location = location.substring(5);
                }
                if (location.endsWith("!/")) {
                    location = location.substring(0, location.length() - 2);
                }
                // Remove a starting / if its a windows server
                if (SystemUtils.IS_OS_WINDOWS && location.startsWith("/")) {
                    location = location.substring(1);
                }

                if (!classpath.contains(location)) {
                    classpath += File.pathSeparator + location;
                }

            }
            return true;
        } catch (Exception e) {
            log.warn("Failed to find location for class {}", className);
            return false;
        }
    }

    /**
     * Compile the script class for a given script code if it is NOT COMPILED YET.
     * 
     * @param scriptCode Script code
     * @return Script class
     * @throws InvalidScriptException Were not able to instantiate or compile a script
     * @throws ElementNotFoundException Script not found
     */
    @ConcurrencyLock
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED) // No TX added so that changes to compiledScripts cache, after calling compileScript(), is seen right away
    public Class<ScriptInterface> getOrCompileScript(String scriptCode) throws ElementNotFoundException, InvalidScriptException {
        CacheKeyStr cacheKey = new CacheKeyStr(currentUser.getProviderCode(), EjbUtils.getCurrentClusterNode() + "_" + scriptCode);

        Class<ScriptInterface> compiledScript = compiledScripts.get(cacheKey);
        if (compiledScript == null) {

            log.warn("Script {} with cache key {} was NOT found in compiled script cache. Currently cache contains {}", scriptCode, cacheKey.toString(), compiledScripts.keySet());

            ScriptInstance script = findByCode(scriptCode, true);
            if (script == null) {
                log.debug("ScriptInstance with {} does not exist", scriptCode);
                throw new ElementNotFoundException(scriptCode, "ScriptInstance");
            } else if (script.isError()) {
                log.debug("ScriptInstance {} failed to compile. Errors: {}", scriptCode, script.getScriptErrors());
                throw new InvalidScriptException(scriptCode, getEntityClass().getName());
            }
            try {
                methodCallingUtils.callCallableInNewTx(() -> compileScript(script.getCode(), script.getSourceTypeEnum(), script.getScript(), script.isActive(), script.isReuse(), false));
            } catch (Exception e) {
                log.error("Failed while compiling script {}", script.getCode(), e);
            }

            compiledScript = compiledScripts.get(cacheKey);
        }

        if (compiledScript == null) {
            log.debug("Failed to retrieve compiled script {}", scriptCode);
            throw new ElementNotFoundException(scriptCode, "ScriptInstance");
        }

        return compiledScript;
    }

    /**
     * Remove compiled script, its logs and cached instances for given script code
     * 
     * @param scriptCode Script code
     */
    @JpaAmpNewTx
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void clearCompiledScript(String scriptCode) {

        CacheKeyStr cacheKey = new CacheKeyStr(currentUser.getProviderCode(), EjbUtils.getCurrentClusterNode() + "_" + scriptCode);

        log.debug("Script {} with cache key {} will be removed from compiled script cache.", scriptCode, cacheKey.toString());

        compiledScripts.remove(cacheKey);
    }

    /**
     * Remove all compiled scripts for a current provider
     */
    @JpaAmpNewTx
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void clearCompiledScripts() {

        String currentProvider = currentUser.getProviderCode();
        log.info("Clear compiled scripts cache for {}/{} ", currentProvider, currentUser);
        Iterator<Entry<CacheKeyStr, Class<ScriptInterface>>> iter = compiledScripts.getAdvancedCache().withFlags(Flag.IGNORE_RETURN_VALUES).entrySet().iterator();
        ArrayList<CacheKeyStr> itemsToBeRemoved = new ArrayList<>();
        while (iter.hasNext()) {
            Entry<CacheKeyStr, Class<ScriptInterface>> entry = iter.next();
            boolean comparison = (entry.getKey().getProvider() == null) ? currentProvider == null : entry.getKey().getProvider().equals(currentProvider);
            if (comparison) {
                itemsToBeRemoved.add(entry.getKey());
            }
        }

        for (CacheKeyStr cacheKey : itemsToBeRemoved) {
            log.debug("Script with cache key {} will be removed from compiled script cache.", cacheKey.toString());
            compiledScripts.getAdvancedCache().withFlags(Flag.IGNORE_RETURN_VALUES).remove(cacheKey);
        }
    }
}