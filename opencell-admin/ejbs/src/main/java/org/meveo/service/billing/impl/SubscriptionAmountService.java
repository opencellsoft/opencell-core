package org.meveo.service.billing.impl;

import org.meveo.model.billing.SubscriptionAmount;
import org.meveo.service.base.PersistenceService;

import javax.ejb.Stateless;

@Stateless
public class SubscriptionAmountService extends PersistenceService<SubscriptionAmount> {
}
