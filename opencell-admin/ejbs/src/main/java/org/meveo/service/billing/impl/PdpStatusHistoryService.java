package org.meveo.service.billing.impl;

import org.meveo.model.billing.PDPStatusHistory;
import org.meveo.service.base.PersistenceService;

import javax.ejb.Stateless;

@Stateless
public class PdpStatusHistoryService extends PersistenceService<PDPStatusHistory> {
}
