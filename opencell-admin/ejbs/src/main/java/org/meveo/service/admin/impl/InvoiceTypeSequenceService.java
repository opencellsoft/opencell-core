package org.meveo.service.admin.impl;

import org.meveo.model.billing.InvoiceTypeSellerSequence;
import org.meveo.service.base.PersistenceService;

import javax.ejb.Stateless;

@Stateless
public class InvoiceTypeSequenceService extends PersistenceService<InvoiceTypeSellerSequence> {
}
