package org.meveo.service.billing.impl;

import static java.lang.Boolean.FALSE;
import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.ZERO;
import static java.util.Arrays.stream;
import static java.util.Collections.emptyList;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import static org.apache.commons.collections4.ListUtils.partition;
import static org.meveo.commons.utils.ParamBean.getInstance;
import static org.meveo.model.billing.InvoiceLineStatusEnum.OPEN;
import static org.meveo.model.billing.InvoiceLineTaxModeEnum.RATE;
import static org.meveo.model.billing.InvoiceStatusEnum.VALIDATED;
import static org.meveo.model.shared.DateUtils.addDaysToDate;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.StringJoiner;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.ws.rs.NotFoundException;

import org.apache.commons.lang3.BooleanUtils;
import org.hibernate.Session;
import org.hibernate.exception.DataException;
import org.meveo.admin.exception.BusinessException;
import org.meveo.admin.job.AggregationConfiguration;
import org.meveo.admin.job.InvoiceLinesFactory;
import org.meveo.api.exception.EntityDoesNotExistsException;
import org.meveo.commons.utils.NumberUtils;
import org.meveo.commons.utils.ParamBeanFactory;
import org.meveo.commons.utils.QueryBuilder;
import org.meveo.commons.utils.StringUtils;
import org.meveo.jpa.EntityManagerProvider;
import org.meveo.jpa.JpaAmpNewTx;
import org.meveo.model.BaseEntity;
import org.meveo.model.DatePeriod;
import org.meveo.model.IBillableEntity;
import org.meveo.model.admin.Seller;
import org.meveo.model.article.AccountingArticle;
import org.meveo.model.billing.AccountingCode;
import org.meveo.model.billing.AdjustmentStatusEnum;
import org.meveo.model.billing.Amounts;
import org.meveo.model.billing.ApplyMinimumModeEnum;
import org.meveo.model.billing.BillingAccount;
import org.meveo.model.billing.BillingEntityTypeEnum;
import org.meveo.model.billing.BillingRun;
import org.meveo.model.billing.ExtraMinAmount;
import org.meveo.model.billing.Invoice;
import org.meveo.model.billing.InvoiceLine;
import org.meveo.model.billing.InvoiceLineTaxModeEnum;
import org.meveo.model.billing.InvoiceLinesGroup;
import org.meveo.model.billing.InvoiceSubCategory;
import org.meveo.model.billing.InvoiceType;
import org.meveo.model.billing.MinAmountData;
import org.meveo.model.billing.MinAmountForAccounts;
import org.meveo.model.billing.MinAmountsResult;
import org.meveo.model.billing.RatedTransaction;
import org.meveo.model.billing.ServiceInstance;
import org.meveo.model.billing.Subscription;
import org.meveo.model.billing.Tax;
import org.meveo.model.billing.ThresholdOptionsEnum;
import org.meveo.model.billing.TradingCurrency;
import org.meveo.model.billing.UserAccount;
import org.meveo.model.catalog.DiscountPlan;
import org.meveo.model.catalog.DiscountPlanItem;
import org.meveo.model.catalog.DiscountPlanItemTypeEnum;
import org.meveo.model.catalog.OfferServiceTemplate;
import org.meveo.model.catalog.OfferTemplate;
import org.meveo.model.catalog.RoundingModeEnum;
import org.meveo.model.catalog.ServiceTemplate;
import org.meveo.model.cpq.CpqQuote;
import org.meveo.model.cpq.Product;
import org.meveo.model.cpq.ProductVersion;
import org.meveo.model.cpq.commercial.CommercialOrder;
import org.meveo.model.cpq.commercial.OrderLot;
import org.meveo.model.cpq.commercial.OrderOffer;
import org.meveo.model.crm.Customer;
import org.meveo.model.crm.IInvoicingMinimumApplicable;
import org.meveo.model.filter.Filter;
import org.meveo.model.jobs.JobExecutionResultImpl;
import org.meveo.model.order.Order;
import org.meveo.model.ordering.OpenOrder;
import org.meveo.model.payments.CustomerAccount;
import org.meveo.model.payments.PaymentMethod;
import org.meveo.model.securityDeposit.SecurityDeposit;
import org.meveo.model.settings.OpenOrderSetting;
import org.meveo.service.admin.impl.SellerService;
import org.meveo.service.admin.impl.TradingCurrencyService;
import org.meveo.service.base.PersistenceService;
import org.meveo.service.billing.impl.article.AccountingArticleService;
import org.meveo.service.catalog.impl.DiscountPlanItemService;
import org.meveo.service.catalog.impl.DiscountPlanService;
import org.meveo.service.catalog.impl.TaxService;
import org.meveo.service.cpq.CpqQuoteService;
import org.meveo.service.cpq.order.CommercialOrderService;
import org.meveo.service.order.OpenOrderService;
import org.meveo.service.payments.impl.PaymentMethodService;
import org.meveo.service.settings.impl.OpenOrderSettingService;
import org.meveo.service.tax.TaxMappingService;
import org.meveo.service.tax.TaxMappingService.TaxInfo;

@Stateless
public class InvoiceLineService extends PersistenceService<InvoiceLine> {

    private static final String INVOICING_PROCESS_TYPE = "InvoiceLine";
    private static final String INVOICE_MINIMUM_COMPLEMENT_CODE = "MIN-STD";

    /**
     * A number of Rated transaction, from which a pending table will be used to update Rated transaction status
     */
    private static int minNrOfRtsToUsePendingTable = -1;

    private static final String UNDERSCORE_SEPARATOR = "_";

    @Inject
    private TaxMappingService taxMappingService;

    @Inject
    private AccountingArticleService accountingArticleService;

    @Inject
    private MinAmountService minAmountService;

    @Inject
    private RatedTransactionService ratedTransactionService;
    
    @Inject
    private CpqQuoteService cpqQuoteService;

    @Inject
    private CommercialOrderService commercialOrderService;

    @Inject
    private BillingAccountService billingAccountService;

    @Inject
    private TaxService taxService;
    
    @Inject
    private AccountingCodeService accountingCodeService;

    @Inject
    private InvoiceService invoiceService;

    @Inject
    private DiscountPlanItemService discountPlanItemService;
    
    @Inject
    private DiscountPlanService discountPlanService;

    @Inject
	private TradingCurrencyService tradingCurrencyService;

    @Inject
    private OpenOrderSettingService openOrderSettingService;

    @Inject
    private OpenOrderService openOrderService;

    @Inject
    private UserAccountService userAccountService;

    @Inject
    private PaymentMethodService paymentMethodService;
    
    @Inject
    private ParamBeanFactory paramBeanFactory;
    
    @Inject
    private UntdidTaxationCategoryService untdidTaxationCategoryService;
    
    @Inject
    private InvoiceTypeService invoiceTypeService;
    
    @Inject
    private BillingRunService billingRunService;
    
    @Inject
    private SellerService sellerService;

    @PostConstruct
    private void init() {
        // Load configuration only once
        if (minNrOfRtsToUsePendingTable == -1) {
            minNrOfRtsToUsePendingTable = paramBeanFactory.getInstance().getPropertyAsInteger("invoicing.minNrOfRtsToUsePendingTable", 1000);
        }
    }

    public List<InvoiceLine> findByQuote(CpqQuote quote) {
        return getEntityManager().createNamedQuery("InvoiceLine.findByQuote", InvoiceLine.class)
                .setParameter("quote", quote)
                .getResultList();
    }

    public List<InvoiceLine> findByCommercialOrder(CommercialOrder commercialOrder) {
        return getEntityManager().createNamedQuery("InvoiceLine.findByCommercialOrder", InvoiceLine.class)
                .setParameter("commercialOrder", commercialOrder)
                .getResultList();
    }
    
    @Override
    public void create(InvoiceLine entity) throws BusinessException {
        createInvoiceLineWithInvoice(entity, entity.getInvoice());
    }
    
    public void createInvoiceLineWithInvoice(InvoiceLine entity, Invoice invoice) throws BusinessException {
        createInvoiceLineWithInvoice(entity, invoice, false);
    }

    public InvoiceLine createInvoiceLineWithInvoiceAndSD(SecurityDeposit securityDepositInput, Invoice invoice, InvoiceLine invoiceLine) throws BusinessException {
        AccountingArticle accountingArticle = accountingArticleService.findByCode("ART_SECURITY_DEPOSIT");
        if(accountingArticle == null) {
            throw new EntityDoesNotExistsException(AccountingArticle.class, "ART_SECURITY_DEPOSIT");
        }
        Boolean isExonerated = securityDepositInput.getBillingAccount().isExoneratedFromtaxes();
        if (isExonerated == null) {
            isExonerated = billingAccountService.isExonerated(securityDepositInput.getBillingAccount());
        }
        TaxInfo recalculatedTaxInfo = taxMappingService.determineTax(accountingArticle.getTaxClass(), 
            securityDepositInput.getSeller(),
            securityDepositInput.getBillingAccount(), null, new Date(), null, isExonerated, false, invoiceLine.getTax());
        BigDecimal amount = securityDepositInput.getAmount();
		calculateTaxAmounts(invoiceLine, recalculatedTaxInfo.tax, amount);          
        invoiceLine.setTaxMode(InvoiceLineTaxModeEnum.ARTICLE);          
        invoiceLine.setAccountingArticle(accountingArticle);
        invoiceLine.setDiscountPlan(null);
        invoiceLine.setUnitPrice(amount);
        invoiceLine.setInvoice(invoice);        
        invoiceLine.setQuantity(new BigDecimal("1"));
        invoiceLine.setLabel("");
        return createInvoiceLineWithInvoice(invoiceLine, invoice, false);
    }

    public InvoiceLine createInvoiceLineWithInvoice(InvoiceLine entity, Invoice invoice, boolean isDuplicated) throws BusinessException {
    	AccountingArticle accountingArticle=entity.getAccountingArticle();
        Date date=new Date();
        if(entity.getValueDate()!=null) {
            date=entity.getValueDate();
        }
        BillingAccount billingAccount = getBillingAccount(invoice, entity);
        Seller seller = getSeller(invoice, entity, billingAccount);
        if (accountingArticle != null && entity.getTax() == null) {
             setApplicableTax(accountingArticle, date, seller, billingAccount, entity);
        }

        super.create(entity);
        if(invoice != null && invoice.getInvoiceLines() != null) {
            invoice.getInvoiceLines().add(entity);
        }
        if(!isDuplicated && entity.getDiscountPlan() != null && entity.getAmountWithoutTax().compareTo(BigDecimal.ZERO)>0 ) {
        	addDiscountPlanInvoice(entity.getDiscountPlan(), entity, billingAccount,invoice, accountingArticle, seller);
        }
		
		return entity;
    }

    private void addDiscountPlanInvoice(DiscountPlan discount, InvoiceLine entity, BillingAccount billingAccount, Invoice invoice, AccountingArticle accountingArticle, Seller seller) {
    	var isDiscountApplicable = discountPlanService.isDiscountPlanApplicable(billingAccount, discount,invoice!=null?invoice.getInvoiceDate():null);
    	if(isDiscountApplicable) {
    		 List<DiscountPlanItem> discountItems = discountPlanItemService.getApplicableDiscountPlanItems(billingAccount, discount, null, invoice!=null?invoice.getInvoiceDate():null, accountingArticle,invoice,entity);
            BigDecimal invoiceLineDiscountAmount = addDiscountPlanInvoiceLine(discountItems, entity, invoice, billingAccount, seller);
            entity.setDiscountAmount(invoiceLineDiscountAmount);
    	}
    }
    
    public BigDecimal addDiscountPlanInvoiceLine(List<DiscountPlanItem> discountItems,InvoiceLine invoiceLine, Invoice invoice, BillingAccount billingAccount, Seller seller) {
        BigDecimal invoiceLineDiscountAmount = BigDecimal.ZERO;
        int rounding = appProvider.getRounding();
        RoundingModeEnum roundingMode = appProvider.getRoundingMode();
        AccountingArticle accountingArticle=null;
	    discountItems.sort(Comparator.comparing(DiscountPlanItem::getFinalSequence));
        for (DiscountPlanItem discountPlanItem : discountItems) {
        	accountingArticle=discountPlanItem.getAccountingArticle();
        	if(accountingArticle==null) {
        		accountingArticle=invoiceLine.getAccountingArticle();
        	}
        	InvoiceLine discountInvoice = new InvoiceLine(invoiceLine, invoice);
        	discountInvoice.setStatus(invoiceLine.getStatus());
                //invoiceLineDiscountAmount = invoiceLineDiscountAmount.add((discountPlanItem.getDiscountValue().divide(hundred)).multiply(entity.getAmountWithoutTax()));
                BigDecimal taxPercent = invoiceLine.getTaxRate();
                if(accountingArticle != null) {
                	TaxInfo taxInfo = taxMappingService.determineTax(accountingArticle.getTaxClass(), seller, billingAccount, null, invoice.getInvoiceDate(),  false, false);
                        taxPercent = taxInfo.tax.getPercent();
                }
                BigDecimal discountAmount = discountPlanItemService.getDiscountAmount(invoiceLine.getUnitPrice(), discountPlanItem,null,invoice,invoiceLine, Collections.emptyList(), null);
                if(discountAmount != null) {
                	invoiceLineDiscountAmount = invoiceLineDiscountAmount.add(discountAmount);
        	  	}
                BigDecimal[] amounts = NumberUtils.computeDerivedAmounts(discountAmount, discountAmount, taxPercent, appProvider.isEntreprise(),  rounding,
                        roundingMode.getRoundingMode());
	            BigDecimal discountValue=discountPlanItemService.getDiscountAmountOrPercent(invoice,invoiceLine, null, invoiceLine.getUnitPrice(), discountPlanItem,null,Collections.emptySet(), null);
                var quantity = invoiceLine.getQuantity();
                discountInvoice.setUnitPrice(discountAmount);
                discountInvoice.setAmountWithoutTax(quantity.compareTo(BigDecimal.ZERO)>0?quantity.multiply(amounts[0]):BigDecimal.ZERO);
                discountInvoice.setAmountWithTax(quantity.multiply(amounts[1]));
                discountInvoice.setDiscountedInvoiceLine(invoiceLine);
                discountInvoice.setAmountTax(quantity.multiply(amounts[2]));
                discountInvoice.setTaxRate(taxPercent);
                discountInvoice.setRawAmount(discountAmount);
                discountInvoice.setAccountingArticle(accountingArticle);
		        discountInvoice.setDiscountPlan(discountPlanItem.getDiscountPlan());
		        discountInvoice.setDiscountPlanItem(discountPlanItem);
		        discountInvoice.setSequence(discountPlanItem.getSequence());
		        discountInvoice.setDiscountPlanType(discountPlanItem.getDiscountPlanItemType());
		        discountInvoice.setDiscountValue(discountValue);
            	super.create(discountInvoice);
		        if(BooleanUtils.isTrue(discountPlanItem.getLastDiscount())){
			        break;
		        }
            
        }
        return invoiceLineDiscountAmount;
    }
     
    
   private void addDiscountPlanInvoiceWithInvoiceSource(DiscountPlan discount, InvoiceLine entity, BillingAccount billingAccount, Invoice invoice, Invoice invoiceSource, AccountingArticle accountingArticle, Seller seller, InvoiceLine invoiceLine) { 
        var isDiscountApplicable = discountPlanService.isDiscountPlanApplicable(billingAccount, discount,invoice!=null?invoice.getInvoiceDate():null);
        if(isDiscountApplicable) {
        	 List<DiscountPlanItem> discountItems = discountPlanItemService.getApplicableDiscountPlanItems(billingAccount, entity.getDiscountPlan(), null, new Date(), accountingArticle,invoice,entity);
             BigDecimal totalDiscountAmount = BigDecimal.ZERO; 
            for (DiscountPlanItem discountPlanItem : discountItems) {
                BigDecimal discountLineAmount = BigDecimal.ZERO;
                InvoiceLine discountInvoice = new InvoiceLine(entity, invoice);
                discountInvoice.setStatus(entity.getStatus());
                if(discountPlanItem.getDiscountPlanItemType() == DiscountPlanItemTypeEnum.FIXED) {
                    totalDiscountAmount = totalDiscountAmount.add(discountPlanItem.getDiscountValue());
                } else {
                    BigDecimal taxPercent = entity.getTaxRate();
                    if(entity.getAccountingArticle() != null) {
                        TaxInfo taxInfo = taxMappingService.determineTax(entity.getAccountingArticle().getTaxClass(), seller, billingAccount, null, invoiceSource.getInvoiceDate(), false, false);
                            taxPercent = taxInfo.tax.getPercent();
                    }
                    BigDecimal discountAmount = discountPlanItemService.getDiscountAmount(entity.getUnitPrice(), discountPlanItem,null,invoice,entity, Collections.emptyList(), null);
                    if(discountAmount != null) {
                        discountLineAmount = discountLineAmount.add(discountAmount);
                    }
                    if(RATE.equals(invoiceLine.getTaxMode())
                            || InvoiceLineTaxModeEnum.TAX.equals(invoiceLine.getTaxMode()))
                    {
                        taxPercent = invoiceLine.getTaxRate();
                    }
                    BigDecimal[] amounts = NumberUtils.computeDerivedAmounts(discountLineAmount, discountLineAmount, taxPercent, appProvider.isEntreprise(), BaseEntity.NB_DECIMALS, RoundingMode.HALF_UP);
                    var quantity = entity.getQuantity();
                    discountInvoice.setUnitPrice(discountLineAmount);
                    discountInvoice.setAmountWithoutTax(quantity.compareTo(BigDecimal.ZERO)>0?quantity.multiply(amounts[0]):BigDecimal.ZERO);
                    discountInvoice.setAmountWithTax(quantity.multiply(amounts[1]));
                    totalDiscountAmount = totalDiscountAmount.add(discountInvoice.getAmountWithoutTax().abs());
                    discountInvoice.setDiscountPlan(null);
                    discountInvoice.setDiscountAmount(BigDecimal.ZERO);
                    discountInvoice.setDiscountedInvoiceLine(entity);
                    discountInvoice.setAmountTax(quantity.multiply(amounts[2]));
                    discountInvoice.setTaxRate(taxPercent);
                    super.create(discountInvoice);
                }
            }
            entity.setDiscountAmount(totalDiscountAmount.compareTo(BigDecimal.ZERO) > 0
                    ? totalDiscountAmount : (totalDiscountAmount.multiply(entity.getQuantity())).abs());
        }
    }

    public List<InvoiceLine> listInvoiceLinesToInvoice(BillingRun billingRun, IBillableEntity entityToInvoice, Date firstTransactionDate,
                                                       Date lastTransactionDate, Filter filter,Map<String, Object> filterParams, int pageSize) throws BusinessException {
			TypedQuery<InvoiceLine> namedQuery = null;
			String byBr = billingRun != null ? "AndBR" : "";
			if (entityToInvoice instanceof Subscription) {
				namedQuery = getEntityManager().createNamedQuery("InvoiceLine.listToInvoiceBySubscription" + byBr, InvoiceLine.class)
						.setParameter("subscriptionId", entityToInvoice.getId());
			} else if (entityToInvoice instanceof BillingAccount) {
				namedQuery = getEntityManager().createNamedQuery("InvoiceLine.listToInvoiceByBillingAccount" + byBr, InvoiceLine.class)
						.setParameter("billingAccountId", entityToInvoice.getId());
			} else if (entityToInvoice instanceof Order) {
				namedQuery = getEntityManager().createNamedQuery("InvoiceLine.listToInvoiceByOrderNumber" + byBr, InvoiceLine.class)
						.setParameter("orderNumber", ((Order) entityToInvoice).getOrderNumber());
			} else if (entityToInvoice instanceof CommercialOrder) {
				namedQuery = getEntityManager().createNamedQuery("InvoiceLine.listToInvoiceByCommercialOrder" + byBr, InvoiceLine.class)
						.setParameter("commercialOrderId", ((CommercialOrder) entityToInvoice).getId());
			} else if (entityToInvoice instanceof CpqQuote) {
				namedQuery = getEntityManager().createNamedQuery("InvoiceLine.listToInvoiceByQuote" + byBr, InvoiceLine.class)
						.setParameter("quoteId", entityToInvoice.getId());
			} else {
				return emptyList();
			}

			if (billingRun != null) {
				namedQuery.setParameter("billingRunId", billingRun.getId());
			} else {
				namedQuery.setParameter("firstTransactionDate", firstTransactionDate)
						.setParameter("lastTransactionDate", lastTransactionDate);
			}
			return namedQuery.setHint("org.hibernate.readOnly", true).setMaxResults(pageSize).getResultList();
    }

    public List<InvoiceLine> listInvoiceLinesByInvoice(long invoiceId) {
        try {
            return getEntityManager().createNamedQuery("InvoiceLine.InvoiceLinesByInvoiceID", InvoiceLine.class)
                    .setParameter("invoiceId", invoiceId)
                    .getResultList();
        } catch (NoResultException e) {
            log.warn("No invoice found for the provided Invoice : " + invoiceId);
            return emptyList();
        }
    }

    public InvoiceLine createInvoiceLine(IBillableEntity entityToInvoice, AccountingArticle accountingArticle, ProductVersion productVersion, OrderLot orderLot, OfferTemplate offerTemplate, OrderOffer orderOffer,
            BigDecimal amountWithoutTaxToBeInvoiced, BigDecimal amountWithTaxToBeInvoiced, BigDecimal taxAmountToBeInvoiced, BigDecimal totalTaxRate) {
        return createInvoiceLine(entityToInvoice, accountingArticle, productVersion, orderLot, offerTemplate, orderOffer, amountWithoutTaxToBeInvoiced, amountWithTaxToBeInvoiced, taxAmountToBeInvoiced, totalTaxRate,
            null);
    }	
    
    public InvoiceLine createInvoiceLine(IBillableEntity entityToInvoice, AccountingArticle accountingArticle, ProductVersion productVersion, OrderLot orderLot, OfferTemplate offerTemplate, OrderOffer orderOffer,
            BigDecimal amountWithoutTaxToBeInvoiced, BigDecimal amountWithTaxToBeInvoiced, BigDecimal taxAmountToBeInvoiced, BigDecimal totalTaxRate, Long discountedInvoiceLineId) {
        return createInvoiceLine(entityToInvoice, accountingArticle, productVersion, orderLot, offerTemplate, orderOffer, amountWithoutTaxToBeInvoiced, amountWithTaxToBeInvoiced, taxAmountToBeInvoiced, totalTaxRate,
            discountedInvoiceLineId, BigDecimal.ONE);
    }
    
    public InvoiceLine createInvoiceLine(IBillableEntity entityToInvoice, AccountingArticle accountingArticle, ProductVersion productVersion, OrderLot orderLot, OfferTemplate offerTemplate, OrderOffer orderOffer,
            BigDecimal amountWithoutTaxToBeInvoiced, BigDecimal amountWithTaxToBeInvoiced, BigDecimal taxAmountToBeInvoiced, BigDecimal totalTaxRate, Long discountedInvoiceLineId, BigDecimal quantity) {
        BillingAccount billingAccount = null;
        InvoiceLine invoiceLine = new InvoiceLine();
        invoiceLine.setAccountingArticle(accountingArticle);
        invoiceLine.setLabel(accountingArticle.getDescription());
        invoiceLine.setProduct(productVersion.getProduct());
        invoiceLine.setProductVersion(productVersion);
        invoiceLine.setOrderLot(orderLot);
        invoiceLine.setOfferTemplate(offerTemplate);
        invoiceLine.setOrderOffer(orderOffer);
        ofNullable(orderOffer).ifPresent(offer -> invoiceLine.setQuoteOffer(offer.getQuoteOffer()));
        if (entityToInvoice instanceof CpqQuote) {
            entityToInvoice = cpqQuoteService.retrieveIfNotManaged((CpqQuote) entityToInvoice);
            CpqQuote quote = ((CpqQuote) entityToInvoice);
            invoiceLine.setQuote(quote);
            billingAccount = quote.getBillableAccount();
            invoiceLine.setBillingAccount(billingAccount);
        }
        CommercialOrder commercialOrder = null;
        if (entityToInvoice instanceof CommercialOrder) {
            entityToInvoice = commercialOrderService.retrieveIfNotManaged((CommercialOrder) entityToInvoice);
            commercialOrder = ((CommercialOrder) entityToInvoice);
            invoiceLine.setCommercialOrder(commercialOrder);
            invoiceLine.setOrderNumber(commercialOrder.getOrderNumber());
            billingAccount = commercialOrder.getBillingAccount();
            invoiceLine.setBillingAccount(billingAccount);
        }
        if (entityToInvoice instanceof BillingAccount) {
            entityToInvoice = billingAccountService.retrieveIfNotManaged((BillingAccount) entityToInvoice);
            billingAccount = ((BillingAccount) entityToInvoice);
            invoiceLine.setBillingAccount(billingAccount);

        }
        invoiceLine.setQuantity(quantity == null || BigDecimal.ZERO.equals(quantity) ? BigDecimal.ONE : quantity);
        amountWithoutTaxToBeInvoiced = (amountWithoutTaxToBeInvoiced != null) ? amountWithoutTaxToBeInvoiced : accountingArticle.getUnitPrice();
        invoiceLine.setUnitPrice(BigDecimal.ZERO.equals(quantity) ? amountWithoutTaxToBeInvoiced : amountWithoutTaxToBeInvoiced.divide(quantity));
        invoiceLine.setAmountWithoutTax(amountWithoutTaxToBeInvoiced);
        invoiceLine.setAmountWithTax(amountWithTaxToBeInvoiced);
        invoiceLine.setAmountTax(taxAmountToBeInvoiced);
        invoiceLine.setTaxRate(totalTaxRate);
        if(discountedInvoiceLineId!=null) {
        	InvoiceLine discountedInvoiceLine=findById(discountedInvoiceLineId);
        	if(discountedInvoiceLine==null) {
        		throw new EntityDoesNotExistsException(InvoiceLine.class, discountedInvoiceLineId);
        	}
        	invoiceLine.setDiscountedInvoiceLine(discountedInvoiceLine);
        }
        invoiceLine.setValueDate(new Date());
        create(invoiceLine);
        commit();
        return invoiceLine;
    }

    private void setApplicableTax(AccountingArticle accountingArticle, Date operationDate, Seller seller, BillingAccount billingAccount, InvoiceLine invoiceLine) {
        if(InvoiceLineTaxModeEnum.ARTICLE.equals(invoiceLine.getTaxMode()))
        {
            Tax taxZero = (billingAccount.isExoneratedFromtaxes() != null && billingAccount.isExoneratedFromtaxes()) ? taxService.getZeroTax() : null;
            Boolean isExonerated = billingAccount.isExoneratedFromtaxes();
            if (isExonerated == null) {
                isExonerated = billingAccountService.isExonerated(billingAccount);
            }
            Object[] applicableTax = taxMappingService
                    .checkIfTaxHasChanged(invoiceLine.getTax(), isExonerated, seller, billingAccount, operationDate, accountingArticle.getTaxClass(), null, taxZero);
            boolean taxRecalculated = (boolean) applicableTax[1];
            if (taxRecalculated) {
                Tax tax = (Tax) applicableTax[0];
                log.debug("Will update invoice line of Billing account {} and tax class {} with new tax from {}/{}% to {}/{}%", billingAccount.getId(),
                        accountingArticle.getTaxClass().getId(), invoiceLine.getTax() == null ? null : invoiceLine.getTax().getId(), tax == null ? null : tax.getPercent(), tax.getId(),
                        tax.getPercent());
                invoiceLine.setTax(tax);
                invoiceLine.setTaxRecalculated(taxRecalculated);
                
                if(!tax.getPercent().equals(invoiceLine.getTaxRate())) {
                       invoiceLine.computeDerivedAmounts(appProvider.isEntreprise(), appProvider.getInvoiceRounding(), appProvider.getInvoiceRoundingMode());
                       invoiceLine.setTaxRate(tax.getPercent());
                }
            }
        }
    }

    public void calculateAmountsAndCreateMinAmountLines(IBillableEntity billableEntity, Date lastTransactionDate, Date invoiceUpToDate,
                                                        boolean calculateAndUpdateTotalAmounts, MinAmountForAccounts minAmountForAccounts) throws BusinessException {
        Amounts totalInvoiceableAmounts;
        List<InvoiceLine> minAmountLines = new ArrayList<>();
        List<ExtraMinAmount> extraMinAmounts = new ArrayList<>();
        Date minRatingDate = addDaysToDate(lastTransactionDate, -1);

        if (billableEntity instanceof Order) {
            if (calculateAndUpdateTotalAmounts) {
                computeTotalOrderInvoiceAmount((Order) billableEntity, new Date(0), lastTransactionDate);
            }
        } else  if (billableEntity instanceof CpqQuote) {
    		if (calculateAndUpdateTotalAmounts) {
    			computeTotalQuoteAmount((CpqQuote) billableEntity, new Date(0), lastTransactionDate);
    		}
    	}else {
            BillingAccount billingAccount =
                    (billableEntity instanceof Subscription) ? ((Subscription) billableEntity).getUserAccount().getBillingAccount() : (BillingAccount) billableEntity;
            Class[] accountClasses = new Class[] { ServiceInstance.class, Subscription.class,
                    UserAccount.class, BillingAccount.class, CustomerAccount.class, Customer.class };
            for (Class accountClass : accountClasses) {
                if (minAmountForAccounts.isMinAmountForAccountsActivated(accountClass, billableEntity)) {
                    MinAmountsResult minAmountsResults = createMinILForAccount(billableEntity, billingAccount,
                            lastTransactionDate, invoiceUpToDate, minRatingDate, extraMinAmounts, accountClass);
                    extraMinAmounts = minAmountsResults.getExtraMinAmounts();
                    minAmountLines.addAll(minAmountsResults.getMinAmountInvoiceLines());
                }
            }
            totalInvoiceableAmounts =
                    minAmountService.computeTotalInvoiceableAmount(billableEntity, new Date(0), lastTransactionDate, invoiceUpToDate, INVOICING_PROCESS_TYPE);
            final Amounts totalAmounts = new Amounts();
            extraMinAmounts.forEach(extraMinAmount -> {
                extraMinAmount.getCreatedAmount().values().forEach(amounts -> {
                    totalAmounts.addAmounts(amounts);
                });
            });
            totalInvoiceableAmounts.addAmounts(totalAmounts);
        }
    }

    private Amounts computeTotalOrderInvoiceAmount(Order order, Date firstTransactionDate, Date lastTransactionDate) {
        String queryString = "InvoiceLine.sumTotalInvoiceableByOrderNumber";
        Query query = getEntityManager().createNamedQuery(queryString)
                .setParameter("orderNumber", order.getOrderNumber())
                .setParameter("firstTransactionDate", firstTransactionDate)
                .setParameter("lastTransactionDate", lastTransactionDate);
        return (Amounts) query.getSingleResult();
    }
    
    private Amounts computeTotalQuoteAmount(CpqQuote quote, Date firstTransactionDate, Date lastTransactionDate) {
        String queryString = "InvoiceLine.sumTotalInvoiceableByQuote";
        Query query = getEntityManager().createNamedQuery(queryString)
                .setParameter("quoteId", quote.getId())
                .setParameter("firstTransactionDate", firstTransactionDate)
                .setParameter("lastTransactionDate", lastTransactionDate);
        return (Amounts) query.getSingleResult();
    }

    private MinAmountsResult createMinILForAccount(IBillableEntity billableEntity, BillingAccount billingAccount,
                                                   Date lastTransactionDate, Date invoiceUpToDate, Date minRatingDate, List<ExtraMinAmount> extraMinAmounts,
                                                   Class accountClass) throws BusinessException {
        MinAmountsResult minAmountsResult = new MinAmountsResult();
        Map<Long, MinAmountData> accountToMinAmount =
                minAmountService.getInvoiceableAmountDataPerAccount(billableEntity, billingAccount, lastTransactionDate, invoiceUpToDate, extraMinAmounts, accountClass, INVOICING_PROCESS_TYPE);
        accountToMinAmount = minAmountService.prepareAccountsWithMinAmount(billableEntity, billingAccount, extraMinAmounts, accountClass, accountToMinAmount);

        for (Map.Entry<Long, MinAmountData> accountAmounts : accountToMinAmount.entrySet()) {
            Map<String, Amounts> minILAmountMap = new HashMap<>();
            if (accountAmounts.getValue() == null || accountAmounts.getValue().getMinAmount() == null) {
                continue;
            }
            BigDecimal minAmount = accountAmounts.getValue().getMinAmount();
            BigDecimal totalInvoiceableAmount =
                    appProvider.isEntreprise() ? accountAmounts.getValue().getAmounts().getAmountWithoutTax() : accountAmounts.getValue().getAmounts().getAmountWithTax();
            IInvoicingMinimumApplicable entity = accountAmounts.getValue().getEntity();
            Seller seller = accountAmounts.getValue().getSeller();
            if (seller == null) {
                throw new BusinessException("Default Seller is mandatory for invoice minimum (Customer.seller)");
            }
            String mapKeyPrefix = seller.getId().toString() + "_";
            BigDecimal diff = minAmount.subtract(totalInvoiceableAmount);
            if (diff.compareTo(BigDecimal.ZERO) <= 0) {
                continue;
            }
            AccountingArticle defaultMinAccountingArticle = getDefaultAccountingArticle();
            if (defaultMinAccountingArticle == null) {
                log.error("No default AccountingArticle defined");
                continue;
            }
            String minAmountLabel = !StringUtils.isBlank(accountAmounts.getValue().getMinAmountLabel())?accountAmounts.getValue().getMinAmountLabel():defaultMinAccountingArticle.getDescription();
            InvoiceSubCategory invoiceSubCategory = defaultMinAccountingArticle.getInvoiceSubCategory();
            String mapKey = mapKeyPrefix + invoiceSubCategory.getId();
            TaxMappingService.TaxInfo taxInfo = taxMappingService
                    .determineTax(defaultMinAccountingArticle.getTaxClass(), seller, billingAccount, null, minRatingDate, null, true, false, null);
            InvoiceLine invoiceLine = createInvoiceLine(minAmountLabel, billableEntity, billingAccount, minRatingDate, entity, seller, defaultMinAccountingArticle, taxInfo, diff);
            minAmountsResult.addMinAmountIL(invoiceLine);
            minILAmountMap.put(mapKey, new Amounts(invoiceLine.getAmountWithoutTax(), invoiceLine.getAmountWithTax(), invoiceLine.getAmountTax()));
            extraMinAmounts.add(new ExtraMinAmount(entity, minILAmountMap));
        }
        minAmountsResult.setExtraMinAmounts(extraMinAmounts);
        return minAmountsResult;
    }


    public AccountingArticle getDefaultAccountingArticle() {
        AccountingArticle accountingArticle = accountingArticleService.findByCode(INVOICE_MINIMUM_COMPLEMENT_CODE);
        if (accountingArticle == null)
            throw new EntityDoesNotExistsException(AccountingArticle.class, INVOICE_MINIMUM_COMPLEMENT_CODE);
        return accountingArticle;
    }

    private InvoiceLine createInvoiceLine( String minAmountLabel, IBillableEntity billableEntity, BillingAccount billingAccount, Date minRatingDate,
                                          IInvoicingMinimumApplicable entity, Seller seller, AccountingArticle defaultAccountingArticle,
                                          TaxMappingService.TaxInfo taxInfo, BigDecimal ilMinAmount) {
        Tax tax = taxInfo.tax;
        BigDecimal[] amounts = NumberUtils.computeDerivedAmounts(ilMinAmount, ilMinAmount, tax.getPercent(), appProvider.isEntreprise(),
                appProvider.getInvoiceRounding(), appProvider.getInvoiceRoundingMode().getRoundingMode());
        InvoiceLine invoiceLine = new InvoiceLine(minRatingDate, BigDecimal.ONE, amounts[0], amounts[1], amounts[2], OPEN,
                billingAccount, minAmountLabel, tax, tax.getPercent(), defaultAccountingArticle);
        if (entity instanceof ServiceInstance) {
            final ServiceInstance serviceInstance = (ServiceInstance) entity;
			invoiceLine.setServiceInstance(serviceInstance);
			final Subscription subscription = serviceInstance.getSubscription();
			if(subscription!=null) {
				invoiceLine.setSubscription(subscription);
				invoiceLine.setUserAccount(subscription.getUserAccount());
			}
        }
        if (entity instanceof Subscription) {
            final Subscription subscription = (Subscription) entity;
			invoiceLine.setSubscription(subscription);
            invoiceLine.setUserAccount(subscription.getUserAccount());
        }
        if (entity instanceof UserAccount) {
            invoiceLine.setUserAccount((UserAccount) entity);
        }
        if (billableEntity instanceof Subscription) {
            invoiceLine.setSubscription((Subscription) billableEntity);
        }
        if(invoiceLine.getSubscription() != null) {
            invoiceLine.getSubscription().setSeller(seller);
        }
        return invoiceLine;
    }

    public MinAmountForAccounts isMinAmountForAccountsActivated(IBillableEntity entity, ApplyMinimumModeEnum applyMinimumModeEnum) {
        return new MinAmountForAccounts(minAmountService.isMinUsed(), entity, applyMinimumModeEnum);
    }

	/**
     * Create Invoice Line using Invoice and InvoiceLineResource
     * @param invoice Invoice to update {@link Invoice}
     * @param invoiceLineResource Invoice Line Resource to convert {@link org.meveo.apiv2.billing.InvoiceLine}
     * @return Invoice Line Converted {@link InvoiceLine}
	 */
	public InvoiceLine create(Invoice invoice, org.meveo.apiv2.billing.InvoiceLine invoiceLineResource) {
		InvoiceLine invoiceLine = new InvoiceLine();
		invoiceLine.setInvoice(invoice);
		invoiceLine = initInvoiceLineFromResource(invoiceLineResource, invoiceLine);
		create(invoiceLine);
		getEntityManager().flush();
		return invoiceLine;
	}

    /**
     * Build Invoice Line from Invoice Line Resource
     * @param invoice Invoice to update {@link Invoice}
     * @param invoiceLineResource Invoice Line Resource to convert {@link org.meveo.apiv2.billing.InvoiceLine}
     * @return Invoice Line Converted {@link InvoiceLine}
     */
    public InvoiceLine getInvoiceLine(Invoice invoice, org.meveo.apiv2.billing.InvoiceLine invoiceLineResource) {
        InvoiceLine invoiceLine = new InvoiceLine();
        invoiceLine.setInvoice(invoice);
        invoiceLine = initInvoiceLineFromResource(invoiceLineResource, invoiceLine);
        return invoiceLine;
    }

    /**
     * Create Invoice Line
     * @param invoiceLine Invoice Line to create {@link InvoiceLine}
     * @return Created Invoice Line {@link InvoiceLine}
     */
    public InvoiceLine createInvoiceLine(InvoiceLine invoiceLine) {
        try {
            create(invoiceLine);
            getEntityManager().flush();
            return invoiceLine;
        } catch (RuntimeException exception) {
            if(exception.getCause() instanceof DataException) {
                Throwable throwable = exception.getCause() != null
                        ? exception.getCause().getCause() : exception;
                throw new BusinessException(throwable.getCause().getMessage());
            } else {
                throw new BusinessException(exception.getMessage());
            }
        }
    }

	public InvoiceLine initInvoiceLineFromResource(org.meveo.apiv2.billing.InvoiceLine resource, InvoiceLine invoiceLine) {
		if(invoiceLine == null) {
			invoiceLine = new InvoiceLine();
		}
		ofNullable(resource.getPrestation()).ifPresent(invoiceLine::setPrestation);
		ofNullable(resource.getQuantity()).ifPresent(invoiceLine::setQuantity);
		ofNullable(resource.getUnitPrice()).ifPresent(invoiceLine::setUnitPrice);
		ofNullable(resource.getDiscountRate()).ifPresent(invoiceLine::setDiscountRate);
		ofNullable(resource.getTaxRate()).ifPresent(invoiceLine::setTaxRate);
		ofNullable(resource.getAmountTax()).ifPresent(invoiceLine::setAmountTax);
		ofNullable(resource.getOrderRef()).ifPresent(invoiceLine::setOrderRef);
		ofNullable(resource.getAccessPoint()).ifPresent(invoiceLine::setAccessPoint);
		ofNullable(resource.getValueDate()).ifPresent(invoiceLine::setValueDate);
		ofNullable(resource.getOrderNumber()).ifPresent(invoiceLine::setOrderNumber);
		ofNullable(resource.getDiscountAmount()).ifPresent(invoiceLine::setDiscountAmount);
		ofNullable(resource.getLabel()).ifPresent(invoiceLine::setLabel);
		ofNullable(resource.getRawAmount()).ifPresent(invoiceLine::setRawAmount);

        if(StringUtils.isNotBlank(resource.getUserAccountCode())) {
            UserAccount userAccount = userAccountService.findByCode(resource.getUserAccountCode());
            if(userAccount == null) {
                throw new EntityDoesNotExistsException(UserAccount.class, resource.getUserAccountCode());
            }
            Invoice invoice = invoiceLine.getInvoice();
            
            if(invoice == null) {
            	if(StringUtils.isNotBlank(resource.getInvoiceId())){
	            	invoice = invoiceService.findById(resource.getInvoiceId());
	            	if(invoice == null) {
	            		throw new NotFoundException("Invoice with id"+resource.getInvoiceId()+" does not exist.");
	            	}
            	}
            }
            if(!invoice.getBillingAccount().getId().equals(userAccount.getBillingAccount().getId())) {
            	throw new BusinessException("You cannot assign the user account with " + userAccount.getBillingAccount() + " to the invoice line, as it differs from the invoice's " + invoice.getBillingAccount());
            }
            invoiceLine.setUserAccount(userAccount);
        } else if (resource.getUserAccountCode() != null) {
            invoiceLine.setUserAccount(null);
        }

        if (StringUtils.isNotBlank(resource.getTaxCode())) {
            Tax tax = taxService.findByCode(resource.getTaxCode());
            if (tax == null) {
                throw new BusinessException("No tax found with code '" + resource.getTaxCode() + "'");
            }
            if (invoiceLine.getTax() != null && tax.getId() != invoiceLine.getTax().getId()) {
                invoiceLine.setTaxRecalculated(true);
            }
            invoiceLine.setTax(tax);
        }
		AccountingArticle accountingArticle=null;
		if (resource.getAccountingArticleCode() != null) {
			 accountingArticle = accountingArticleService.findByCode(resource.getAccountingArticleCode());
		}	
		
		if(resource.getTaxMode() != null) {   
            try {
                invoiceLine.setTaxMode(InvoiceLineTaxModeEnum.valueOf(resource.getTaxMode().toUpperCase()));
            } catch (IllegalArgumentException e) {
                log.error("the Value tax Mode " + resource.getTaxMode() +" is not in ('ARTICLE', 'TAX', 'RATE')", e);
                throw new BusinessException("the Value tax Mode " + resource.getTaxMode() +" is not in ('ARTICLE', 'TAX', 'RATE')");
            }
        }
		
		if(invoiceLine.getQuantity() == null) {
            invoiceLine.setQuantity(new BigDecimal(1));
        }

		invoiceLine.setConversionFromBillingCurrency(false);
        if (StringUtils.isNotBlank(resource.getUnitPriceCurrency())) {
            String tradingCurrency = invoiceLine.getInvoice().getTradingCurrency()!= null ? invoiceLine.getInvoice().getTradingCurrency().getCurrencyCode() : null;
            String functionalCurrency = appProvider.getCurrency() != null ? appProvider.getCurrency().getCurrencyCode() : null;
        	
            if (!(resource.getUnitPriceCurrency().equals(tradingCurrency) || resource.getUnitPriceCurrency().equals(functionalCurrency))) {
        		throw new BusinessException("currency should be equals to billing currency or functional currency");
        	}
			
            if (resource.getUnitPriceCurrency().equals(tradingCurrency)) {
				BigDecimal appliedRate = invoiceLine.getInvoice() != null ? invoiceLine.getInvoice().getAppliedRate() : ONE;
				invoiceLine.setUnitPrice(resource.getUnitPrice().divide(appliedRate, BaseEntity.NB_DECIMALS, RoundingMode.HALF_UP));
				invoiceLine.setConversionFromBillingCurrency(true);
			}
        }

        BigDecimal discountAmount = BigDecimal.ZERO;
        if(invoiceLine.getDiscountRate() != null && invoiceLine.getUnitPrice().compareTo(BigDecimal.ZERO) > 0) {
            BigDecimal discountRate = invoiceLine.getDiscountRate().divide(new BigDecimal(100), appProvider.getRounding(),
                    appProvider.getRoundingMode().getRoundingMode());
            discountAmount = invoiceLine.getUnitPrice().multiply(discountRate);
            invoiceLine.setDiscountAmount(discountAmount);
        }
        if(invoiceLine.getUnitPrice() == null) {
            if (accountingArticle != null && accountingArticle.getUnitPrice() != null) {
                invoiceLine.setUnitPrice(accountingArticle.getUnitPrice());
                if(resource.getQuantity() != null) {
                    invoiceLine.setAmountWithoutTax((accountingArticle.getUnitPrice().multiply(resource.getQuantity())).subtract(discountAmount));
                    invoiceLine.setAmountWithTax((accountingArticle.getUnitPrice().multiply(resource.getQuantity())).subtract(discountAmount));
                }
            } else {
                throw new BusinessException("You cannot create an invoice line without a price " +
                        "if unit price is not set on article with code : " + resource.getAccountingArticleCode());
            }
        } else {
            invoiceLine.setAmountWithoutTax((invoiceLine.getUnitPrice().multiply(resource.getQuantity())).subtract(discountAmount));
            invoiceLine.setAmountWithTax(NumberUtils.computeTax(invoiceLine.getAmountWithoutTax(),
                    invoiceLine.getTaxRate(), appProvider.getInvoiceRounding(), appProvider.getInvoiceRoundingMode().getRoundingMode()).add(invoiceLine.getAmountWithoutTax()));
        }

		if(resource.getServiceInstanceCode()!=null) {
			invoiceLine.setServiceInstance((ServiceInstance)tryToFindByEntityClassAndCode(ServiceInstance.class, resource.getServiceInstanceCode()));
		}
		if(resource.getSubscriptionCode()!=null) {
			invoiceLine.setSubscription((Subscription)tryToFindByEntityClassAndCode(Subscription.class, resource.getSubscriptionCode()));
		}
		if(resource.getProductCode()!=null) {
			invoiceLine.setProduct((Product)tryToFindByEntityClassAndCode(Product.class, resource.getProductCode()));
		}
		if(resource.getAccountingArticleCode()!=null) {
			invoiceLine.setAccountingArticle((AccountingArticle)tryToFindByEntityClassAndCode(AccountingArticle.class, resource.getAccountingArticleCode()));
		}
		if(resource.getServiceTemplateCode()!=null) {
			invoiceLine.setServiceTemplate((ServiceTemplate)tryToFindByEntityClassAndCode(ServiceTemplate.class, resource.getServiceTemplateCode()));
		}
		if(resource.getDiscountPlanCode()!=null) {
			invoiceLine.setDiscountPlan((DiscountPlan)tryToFindByEntityClassAndCode(DiscountPlan.class, resource.getDiscountPlanCode()));
		}
		if(resource.getTaxCode()!=null) {
			invoiceLine.setTax((Tax)tryToFindByEntityClassAndCode(Tax.class, resource.getTaxCode()));
			invoiceLine.setTaxRate(invoiceLine.getTax().getPercent());
		}
		if(resource.getOrderLotCode()!=null) {
			invoiceLine.setOrderLot((OrderLot)tryToFindByEntityClassAndCode(OrderLot.class, resource.getOrderLotCode()));
		}
		if(resource.getBillingAccountCode()!=null) {
			invoiceLine.setBillingAccount((BillingAccount)tryToFindByEntityClassAndCode(BillingAccount.class, resource.getBillingAccountCode()));
        } else if(invoiceLine.getInvoice()!=null){
        	invoiceLine.setBillingAccount(invoiceLine.getInvoice().getBillingAccount());
        }
        if (resource.getOfferTemplateCode() != null) {
            invoiceLine.setOfferTemplate((OfferTemplate) tryToFindByEntityClassAndCode(OfferTemplate.class, resource.getOfferTemplateCode()));
        }

        if (resource.isTaxRecalculated() != null) {
            invoiceLine.setTaxRecalculated(resource.isTaxRecalculated());
        }

        var datePeriod = new DatePeriod();
        if (resource.getStartDate() != null) {
            datePeriod.setFrom(resource.getStartDate());
        }
        if (resource.getEndDate() != null) {
            datePeriod.setTo(resource.getEndDate());
        }
        if(resource.getLabel()==null && invoiceLine.getAccountingArticle()!=null) {
            invoiceLine.setLabel(!StringUtils.isBlank(invoiceLine.getAccountingArticle().getDescription())?invoiceLine.getAccountingArticle().getDescription():invoiceLine.getAccountingArticle().getCode());
        }

		if(invoiceLine.getTax()==null  && accountingArticle != null
                && invoiceLine.getBillingAccount() != null && !invoiceLine.getTaxMode().equals(RATE))  {
			BillingAccount  billingAccount = billingAccountService.refreshOrRetrieve(invoiceLine.getBillingAccount());
			Boolean isExonerated = billingAccount.isExoneratedFromtaxes();
	        if (isExonerated == null) {
	            isExonerated = billingAccountService.isExonerated(billingAccount);
	        }

            Seller billingAccountSeller = billingAccount.getCustomerAccount().getCustomer() != null ? billingAccount.getCustomerAccount().getCustomer().getSeller() : null;
            Seller seller = billingAccountSeller == null ? invoiceLine.getInvoice().getSeller() : billingAccountSeller;

	        TaxInfo recalculatedTaxInfo = taxMappingService.determineTax(accountingArticle.getTaxClass(), 
	            seller,
	            billingAccount, null, 
	            invoiceLine.getValueDate()!=null?invoiceLine.getValueDate():new Date(), null, 
	                    isExonerated, false, invoiceLine.getTax());
	        invoiceLine.setTax(recalculatedTaxInfo.tax);
	        invoiceLine.setTaxRate(recalculatedTaxInfo.tax.getPercent());
            invoiceLine.setAmountWithTax(NumberUtils.computeTax(invoiceLine.getAmountWithoutTax(),
                    invoiceLine.getTaxRate(), appProvider.getInvoiceRounding(), appProvider.getInvoiceRoundingMode().getRoundingMode()).add(invoiceLine.getAmountWithoutTax()));
		}
        if(!appProvider.isEntreprise()) {
            BigDecimal[] amounts = computeAmounts(invoiceLine.getUnitPrice(),
                    invoiceLine.getQuantity(), invoiceLine.getTaxRate(), appProvider.isEntreprise());
            invoiceLine.setAmountWithoutTax(amounts[0]);
            invoiceLine.setAmountWithTax(amounts[1]);
            invoiceLine.setAmountTax(amounts[2]);
        }
		
        /****recalculate amountWithoutTax and amountWithTax  according to tax percent and the business model (b2b or b2c)*/
        final BigDecimal appliedRate = invoiceLine.getInvoice() != null ? invoiceLine.getInvoice().getAppliedRate() : ONE;
        BigDecimal[] amounts = computeAmounts(invoiceLine.getUnitPrice(), appliedRate,
                invoiceLine.getQuantity(), invoiceLine.getTaxRate(), appProvider.isEntreprise());
        invoiceLine.setAmountWithoutTax(amounts[0]);
        invoiceLine.setAmountWithTax(amounts[1]);
        invoiceLine.setAmountTax(amounts[2]);
        invoiceLine.setTransactionalAmountWithoutTax(amounts[3]);
        invoiceLine.setTransactionalAmountWithTax(amounts[4]);
        invoiceLine.setTransactionalAmountTax(amounts[5]);

        invoiceLine.setValidity(datePeriod);
        invoiceLine.setProductVersion((ProductVersion) tryToFindByEntityClassAndId(ProductVersion.class, resource.getProductVersionId()));
        invoiceLine.setOfferServiceTemplate((OfferServiceTemplate) tryToFindByEntityClassAndId(OfferServiceTemplate.class, resource.getOfferServiceTemplateId()));
        invoiceLine.setCommercialOrder((CommercialOrder) tryToFindByEntityClassAndId(CommercialOrder.class, resource.getCommercialOrderId()));
        invoiceLine.setBillingRun((BillingRun) tryToFindByEntityClassAndId(BillingRun.class, resource.getBillingRunId()));

        if(resource.getTaxMode() != null && resource.getTaxMode().equals(RATE.name())) {
            invoiceLine.setTax(findTaxByTaxRateAndAccountingCode(resource.getTaxRate(), resource.getTaxAccountingCode()));
            invoiceLine.setTaxRate(resource.getTaxRate());
        }
        
        if(invoiceLine.getInvoice()!=null) {
            InvoiceType invoiceType = invoiceLine.getInvoice().getInvoiceType();
            String invoiceTypeCode = invoiceType.getCode();
            String accountingArticleCode = accountingArticle.getCode();
            if("SECURITY_DEPOSIT".equals(invoiceTypeCode) && !"ART_SECURITY_DEPOSIT".equals(accountingArticleCode)) {
                throw new BusinessException("accountingArticleCode should be 'ART_SECURITY_DEPOSIT'");
            }
        }
        invoiceLine.setInvoiceRounding(appProvider.getInvoiceRounding());
        invoiceLine.setRoundingMode(appProvider.getInvoiceRoundingMode());
        InvoiceLine.setRoundingConfig(appProvider.getInvoiceRounding(), appProvider.getInvoiceRoundingMode());

        return invoiceLine;
    }

    private BigDecimal[] computeAmounts(BigDecimal unitPrice,
                                        BigDecimal quantity, BigDecimal taxRate, boolean isEnterprise) {
        BigDecimal amountTax;
        BigDecimal amountWithTax;
        BigDecimal amountWithoutTax;
        final BigDecimal hundred = new BigDecimal(100);
        if (isEnterprise) {
            amountWithoutTax = unitPrice.multiply(quantity);
            amountTax = (amountWithoutTax.multiply(taxRate))
                    .divide(hundred, appProvider.getRounding(), appProvider.getRoundingMode().getRoundingMode());
            amountWithTax = amountWithoutTax.add(amountTax);
        } else {
            amountWithTax = unitPrice.multiply(quantity);
            BigDecimal computeValue = (hundred.add(taxRate))
                    .divide(hundred, appProvider.getRounding(), appProvider.getRoundingMode().getRoundingMode());
            amountWithoutTax = amountWithTax
                    .divide(computeValue, appProvider.getRounding(), appProvider.getRoundingMode().getRoundingMode());
            amountTax = amountWithTax.subtract(amountWithoutTax);
        }
        return new BigDecimal[]{amountWithoutTax, amountWithTax, amountTax};
    }

	
	/**
     * get entity Taxe by TaxeRate and Accounting Code
     * 
	 * @param taxRate
	 * @param accountingCodeStr
	 * @return Tax
	 */
	private Tax findTaxByTaxRateAndAccountingCode(BigDecimal taxRate, String accountingCodeStr) {
        AccountingCode accountingCode = null;
        String parCodeAccountingCode = "";

        if(accountingCodeStr != null) {
            accountingCode = Optional.ofNullable(accountingCodeService.findByCode(accountingCodeStr))
                .orElseThrow(() -> new EntityDoesNotExistsException(AccountingCode.class, accountingCodeStr));

            parCodeAccountingCode = "_" + accountingCodeStr;
        }
        Tax tax = taxService.findTaxByRateAndAccountingCode(taxRate, accountingCode);
        if(tax == null) {
            tax = new Tax();
            tax.setPercent(taxRate);
            tax.setCode("TAX_" + taxRate + parCodeAccountingCode);
            tax.setUuid("TAX_" + taxRate + parCodeAccountingCode);
            tax.setAccountingCode(accountingCode);
            tax.setUntdidTaxationCategory(untdidTaxationCategoryService.getByCode("S"));
            taxService.create(tax);
        }
        return tax;
    }

    private BigDecimal[] computeAmounts(BigDecimal unitPrice, BigDecimal appliedRate,
                                        BigDecimal quantity, BigDecimal taxRate, boolean isEnterprise) {
        BigDecimal amountTax;
        BigDecimal amountWithTax;
        BigDecimal amountWithoutTax;
        BigDecimal transactionalAmountTax;
        BigDecimal transactionalAmountWithTax;
        BigDecimal transactionalAmountWithoutTax;
        final BigDecimal hundred = new BigDecimal(100);
        final BigDecimal transactionalUnitPrice = unitPrice.multiply(appliedRate).setScale(appProvider.getRounding(),
                appProvider.getRoundingMode().getRoundingMode());
        if(isEnterprise) {
            amountWithoutTax = unitPrice.multiply(quantity);
            amountTax = (amountWithoutTax.multiply(taxRate))
                    .divide(hundred, appProvider.getRounding(), appProvider.getRoundingMode().getRoundingMode());
            amountWithTax = amountWithoutTax.add(amountTax);

            transactionalAmountWithoutTax = transactionalUnitPrice.multiply(quantity);
            transactionalAmountTax = (transactionalAmountWithoutTax.multiply(taxRate))
                    .divide(hundred, appProvider.getRounding(), appProvider.getRoundingMode().getRoundingMode());
            transactionalAmountWithTax = transactionalAmountWithoutTax.add(transactionalAmountTax);
        } else {
            BigDecimal computeValue = (hundred.add(taxRate))
                    .divide(hundred, appProvider.getRounding(), appProvider.getRoundingMode().getRoundingMode());
            amountWithTax = unitPrice.multiply(quantity);
            amountWithoutTax = amountWithTax
                    .divide(computeValue, appProvider.getRounding(), appProvider.getRoundingMode().getRoundingMode());
            amountTax = amountWithTax.subtract(amountWithoutTax);

            transactionalAmountWithTax = transactionalUnitPrice.multiply(quantity);
            transactionalAmountWithoutTax = transactionalAmountWithTax
                    .divide(computeValue, appProvider.getRounding(), appProvider.getRoundingMode().getRoundingMode());
            transactionalAmountTax = transactionalAmountWithTax.subtract(transactionalAmountWithoutTax);
        }
        return new BigDecimal[]{amountWithoutTax, amountWithTax, amountTax,
                transactionalAmountWithoutTax, transactionalAmountWithTax, transactionalAmountTax};
    }

    private BigDecimal getRateForInvoice(InvoiceLine invoiceLine) {
        if (null != appProvider.getCurrency()) {
            TradingCurrency providerTradingCurrency = tradingCurrencyService.findByTradingCurrencyCode(appProvider.getCurrency().getCurrencyCode());

            Invoice invoice = invoiceService.findById(invoiceLine.getInvoice().getId());
            if (invoice.getTradingCurrency() != null) {
                TradingCurrency invoiceTradingCurrency = tradingCurrencyService.findById(invoice.getTradingCurrency().getId());
                if (invoiceTradingCurrency.getCurrentRate() != null
                        && invoiceTradingCurrency.getId() != providerTradingCurrency.getId()
                        && invoiceTradingCurrency.getCurrentRate() != BigDecimal.ZERO) {
                    return invoiceTradingCurrency.getCurrentRate();
                }

            }

            if (invoice.getBillingAccount() != null && invoice.getBillingAccount().getTradingCurrency() != null  ) {

                    TradingCurrency billingAccountTradingCurrency = tradingCurrencyService.findById(invoice.getBillingAccount().getTradingCurrency().getId());
                    if (billingAccountTradingCurrency.getCurrentRate() != null
                            && billingAccountTradingCurrency.getId() != providerTradingCurrency.getId()
                            && billingAccountTradingCurrency.getCurrentRate() != BigDecimal.ZERO) {
                        return billingAccountTradingCurrency.getCurrentRate();
                    }


                }

        }

        return BigDecimal.ONE;

    }

	/**
     * Update Invoice Line
	 * @param invoice Invoice to update
	 * @param invoiceLineResource Invoice Line Resource to update {@link org.meveo.apiv2.billing.InvoiceLine}
	 * @param invoiceLineId Invoice Line Id
	 */
	public void updateInvoiceLineByInvoice(Invoice invoice, org.meveo.apiv2.billing.InvoiceLine invoiceLineResource, Long invoiceLineId) {
		InvoiceLine invoiceLine = findInvoiceLine(invoice, invoiceLineId);
		invoiceLine = initInvoiceLineFromResource(invoiceLineResource, invoiceLine);
		update(invoiceLine);
	}

    /**
     * Get Invoice Line to update
     * @param invoice Invoice to update
     * @param invoiceLineId Invoice Line Id
     * @return Invoice Line to update {@link InvoiceLine}
     */
	public InvoiceLine findInvoiceLine(Invoice invoice, Long invoiceLineId) {
		InvoiceLine invoiceLine = findById(invoiceLineId);
		if(!invoice.equals(invoiceLine.getInvoice())) {
			throw new BusinessException("invoice line with ID "+invoiceLineId+" is not related to invoice with id:"+invoice.getId());
		}
		return invoiceLine;
	}
	
	/**
	 * @param invoice
	 * @param lineId invoiceLine id
	 */
	public void remove(Invoice invoice, Long lineId) {
		InvoiceLine invoiceLine = findInvoiceLine(invoice, lineId);
        reduceDiscountAmounts(invoice, invoiceLine);
        deleteByDiscountedPlan(invoiceLine);
        if(invoiceLine.getRatedTransactions() != null) {
            List<Long> ids = invoiceLine.getRatedTransactions()
                    .stream()
                    .map(RatedTransaction::getId)
                    .collect(toList());
            ratedTransactionService.reopenRatedTransaction(ids);
        }
        remove(invoiceLine);
	}

    private void reduceDiscountAmounts(Invoice invoice, InvoiceLine invoiceLine) {
        if (invoiceLine.getDiscountPlan() != null
                && invoiceLine.getDiscountAmount() != null
                && !invoiceLine.getDiscountAmount().equals(BigDecimal.ZERO)) {
            invoice.setDiscountAmount(invoice.getDiscountAmount().subtract(invoiceLine.getDiscountAmount()));
        }
        if (invoice.getAmountWithoutTaxBeforeDiscount() != null
                && invoice.getAmountWithoutTaxBeforeDiscount().compareTo(invoice.getAmountWithoutTax()) != 0
                && invoice.getAmountWithoutTaxBeforeDiscount().compareTo(BigDecimal.ZERO) > 0
                && invoiceLine.getAmountWithoutTax().compareTo(BigDecimal.ZERO) > 0) {
            invoice.setAmountWithoutTaxBeforeDiscount(
                    invoice.getAmountWithoutTaxBeforeDiscount().subtract(invoiceLine.getAmountWithoutTax()));
        }
        if (invoiceLine.getDiscountedInvoiceLine() != null
                && invoiceLine.getAmountWithoutTax().compareTo(BigDecimal.ZERO) < 0) {
            InvoiceLine discountedInvoiceLine = invoiceLine.getDiscountedInvoiceLine();
            discountedInvoiceLine.setDiscountAmount(
                    discountedInvoiceLine.getDiscountAmount().add(invoiceLine.getAmountWithoutTax()));
            update(discountedInvoiceLine);
            invoice.setDiscountAmount(invoice.getDiscountAmount().subtract(discountedInvoiceLine.getDiscountAmount()));
        }
    }

    public List<Object[]> getTotalPositiveILAmountsByBR(BillingRun billingRun) {
        return getEntityManager().createNamedQuery("InvoiceLine.sumPositiveILByBillingRun")
                .setParameter("billingRunId", billingRun.getId())
                .getResultList();
    }

    public void uninvoiceILs(Collection<Long> invoicesIds) {
        getEntityManager().createNamedQuery("InvoiceLine.unInvoiceByInvoiceIds")
                .setParameter("now", new Date())
                .setParameter("invoiceIds", invoicesIds)
                .executeUpdate();

    }

    public void cancelIlByInvoices(Collection<Long> invoicesIds) {
        getEntityManager().createNamedQuery("InvoiceLine.cancelByInvoiceIds")
                .setParameter("now", new Date())
                .setParameter("invoicesIds", invoicesIds)
                .executeUpdate();
    }

    public void cancelIlForRemoveByInvoices(Collection<Long> invoicesIds) {
        getEntityManager().createNamedQuery("InvoiceLine.cancelForRemoveByInvoiceIds")
                .setParameter("now", new Date())
                .setParameter("invoicesIds", invoicesIds)
                .executeUpdate();
    }

    /**
     * Get a list of invoiceable Invoice Liness for a given BllingAccount and a list of ids
     *
     * @param billingAccountId
     * @param ids
     *
     * @return A list of InvoiceLine entities
     * @throws BusinessException General exception
     */
	public List<InvoiceLine> listByBillingAccountAndIDs(Long billingAccountId, Set<Long> ids) throws BusinessException {
		return getEntityManager().createNamedQuery("InvoiceLine.listToInvoiceByBillingAccountAndIDs", InvoiceLine.class)
				.setParameter("billingAccountId", billingAccountId).setParameter("listOfIds", ids).getResultList();
	}

	/**
	 * @param invoiceSubCategory
	 * @return
	 */
	public List<InvoiceLine> findOpenILbySubCat(InvoiceSubCategory invoiceSubCategory) {
        QueryBuilder qb = new QueryBuilder("select il from InvoiceLine il ", "il");
        if (invoiceSubCategory != null) {
            qb.addCriterionEntity("il.accountingArticle.invoiceSubCategory", invoiceSubCategory);
        }
        qb.addSql("il.status='OPEN'");

        try {
            return qb.getQuery(getEntityManager()).getResultList();
        } catch (NoResultException e) {
            return new ArrayList<>();
        }
	}

    /**
     * Create Query builder from a map of filters
     * filters : Map of filters
     * Return : QueryBuilder
     */
    public QueryBuilder fromFilters(Map<String, Object> filters) {
    	return getQueryFromFilters(filters, null, null, false);
    }
    	
    	
    /**
     * Retrieve invoice lines associated to an invoice
     *
     * @param invoice Invoice
     * @return A list of invoice Lines
     */
    public List<InvoiceLine> getInvoiceLinesByInvoice(Invoice invoice, boolean includeFree) {
        if (invoice.getId() == null) {
            return new ArrayList<>();
        }
        if (includeFree) {
            return getEntityManager().createNamedQuery("InvoiceLine.listByInvoice", InvoiceLine.class)
                    .setParameter("invoice", invoice)
                    .getResultList();
        } else {
            return getEntityManager().createNamedQuery("InvoiceLine.listByInvoiceNotFree", InvoiceLine.class)
                    .setParameter("invoice", invoice)
                    .getResultList();
        }
    }
    
    public InvoiceLineCreationStatistics createInvoiceLines(List<Map<String, Object>> groupedRTs, AggregationConfiguration configuration, String openOrderNumber) throws BusinessException {
        return createInvoiceLines(groupedRTs, configuration, null, null, openOrderNumber);
    }
    
    @SuppressWarnings("unchecked")
    public InvoiceLineCreationStatistics createInvoiceLines(List<Map<String, Object>> groupedRTs, AggregationConfiguration configuration, JobExecutionResultImpl result, BillingRun billingRun, String openOrderNumber)
            throws BusinessException {
    	
        InvoiceLinesFactory linesFactory = new InvoiceLinesFactory();
        Map<Long, Long> iLIdsRtIdsCorrespondence = new HashMap<>();

        InvoiceLineCreationStatistics statistics = new InvoiceLineCreationStatistics();

        OpenOrderSetting openOrderSetting = openOrderSettingService.findLastOne();
        boolean useOpenOrder = ofNullable(openOrderSetting).map(OpenOrderSetting::getUseOpenOrders).orElse(false);
        InvoiceLine invoiceLine=null;
        List<Long> associatedRtIds;

        Long billingRunId = null;
        boolean incrementalInvoiceLines = false;
        if (billingRun != null) {
            billingRunId = billingRun.getId();
            incrementalInvoiceLines = billingRun.getIncrementalInvoiceLines();
        }

        Object[][] rtIlBrIds = new Object[groupedRTs.size()][3];
        int numberOrRts = 0;
        
        List<InvoiceLine> invoiceLines = new ArrayList<>();

        int i = 0;
	    BillingAccount billingAccount = null;
        for (Map<String, Object> groupedRT : groupedRTs) {
            Long invoiceLineId = null;
			billingAccount = billingAccountService.findById(Long.valueOf(groupedRT.get("billing_account__id").toString()));
            if (groupedRT.get("rated_transaction_ids") instanceof Number) {
                associatedRtIds = new ArrayList<>();
                associatedRtIds.add(((Number) groupedRT.get("rated_transaction_ids")).longValue());
            } else {
                associatedRtIds = stream(((String) groupedRT.get("rated_transaction_ids")).split(",")).map(Long::parseLong).collect(toList());
            }

            if (incrementalInvoiceLines && groupedRT.get("invoice_line_id") != null) {
                invoiceLineId = ((Number) groupedRT.get("invoice_line_id")).longValue();
                BigDecimal amountWithoutTax = ofNullable(((BigDecimal) groupedRT.get("amount_without_tax"))).orElse(ZERO).add((BigDecimal) groupedRT.get("sum_without_tax"));
                BigDecimal amountWithTax = ofNullable(((BigDecimal) groupedRT.get("amount_with_tax"))).orElse(ZERO).add((BigDecimal) groupedRT.get("sum_with_tax"));
                BigDecimal taxPercent = ofNullable((BigDecimal) groupedRT.get("tax_rate")).orElse((BigDecimal) groupedRT.get("tax_percent"));
                BigDecimal[] amounts = NumberUtils.computeDerivedAmounts(amountWithoutTax, amountWithTax, taxPercent, appProvider.isEntreprise(), appProvider.getRounding(),
                        appProvider.getRoundingMode().getRoundingMode());
                BigDecimal deltaAmountWithoutTax = (BigDecimal) groupedRT.get("sum_without_tax");
                BigDecimal deltaAmountWithTax = (BigDecimal) groupedRT.get("sum_with_tax");
                BigDecimal deltaAmountTax = deltaAmountWithTax.subtract(deltaAmountWithoutTax);
                BigDecimal[] deltaAmounts = new BigDecimal[] {deltaAmountWithoutTax, deltaAmountWithTax, deltaAmountTax};

                BigDecimal deltaQuantity = (BigDecimal) groupedRT.get("quantity");
                BigDecimal quantity = ((BigDecimal) groupedRT.get("accumulated_quantity")).add((BigDecimal) groupedRT.get("quantity"));
                Date beginDate = (Date) groupedRT.get("begin_date");
                if (groupedRT.get("start_date") != null && (beginDate == null || ((Date) groupedRT.get("start_date")).before(beginDate))) {
                    beginDate = (Date) groupedRT.get("start_date");
                }
                Date finishDate = (Date) groupedRT.get("finish_date");
                if (groupedRT.get("end_date") != null && (finishDate == null || ((Date) groupedRT.get("end_date")).after(finishDate))) {
                    finishDate = (Date) groupedRT.get("end_date");
                }

                BigDecimal unitPrice = (BigDecimal) groupedRT.get("unit_price");
                if (billingRun.getBillingCycle() != null && !billingRun.getBillingCycle().isDisableAggregation() && billingRun.getBillingCycle().isAggregateUnitAmounts()) {
                    unitPrice = quantity.compareTo(ZERO) == 0 ? amountWithoutTax : amountWithoutTax.divide(quantity,
                            appProvider.getRounding(), appProvider.getRoundingMode().getRoundingMode());
                }
	            invoiceLine = this.findById(invoiceLineId);
	            invoiceLine.setSeller(ofNullable(groupedRT.get("seller_id")).map(id -> sellerService.getEntityManager().getReference(Seller.class, ((Number)id).longValue())).orElse(null));
	            invoiceLine.setAmountWithoutTax(invoiceLine.getAmountWithoutTax().add(deltaAmountWithoutTax));
	            invoiceLine.setAmountWithTax(invoiceLine.getAmountWithTax().add(deltaAmountWithTax));
	            invoiceLine.setAmountTax(invoiceLine.getAmountTax().add(deltaAmountTax));
	            invoiceLine.setQuantity(invoiceLine.getQuantity().add(deltaQuantity));
	            invoiceLine.setValidity(new DatePeriod(beginDate, finishDate));
	            invoiceLine.setUnitPrice(unitPrice);
				this.update(invoiceLine);
                //linesFactory.update(invoiceLineId, deltaAmounts, deltaQuantity, beginDate, finishDate, unitPrice);
                statistics.addToAmountWithoutTax(amounts[0]);
                statistics.addToAmountWithTax(amounts[1]);
                statistics.addToAmountTax(amounts[2]);

            } else {
                invoiceLine = linesFactory.create(groupedRT, iLIdsRtIdsCorrespondence, configuration, result, appProvider, billingRun, openOrderNumber);
                statistics.addToAmountWithTax(invoiceLine.getAmountWithTax());
                statistics.addToAmountWithoutTax(invoiceLine.getAmountWithoutTax());
                statistics.addToAmountTax(invoiceLine.getAmountTax());
                invoiceLineId = invoiceLine.getId();

                for (Long id : associatedRtIds) {
                    iLIdsRtIdsCorrespondence.put(id, invoiceLine.getId());
                    }

                if(useOpenOrder) {
                    OpenOrder openOrder = openOrderService.findOpenOrderCompatibleForIL(invoiceLine, configuration);
                    if (openOrder != null) {
                        invoiceLine.setOpenOrderNumber(openOrder.getOpenOrderNumber());
                        openOrder.setBalance(openOrder.getBalance().subtract(invoiceLine.getAmountWithoutTax()));
                    }
                }
            }
	        
	        buildInvoiceKey(invoiceLine, billingRun);
	        invoiceLines.add(invoiceLine);
			
            rtIlBrIds[i][0] = associatedRtIds;
            rtIlBrIds[i][1] = invoiceLine;
            rtIlBrIds[i][2] = billingRunId;
            numberOrRts = numberOrRts + associatedRtIds.size();
            i++;
        }
        
        List<InvoiceLinesGroup> customInvoiceLinesGroups = billingRun != null ? executeBillingCycleScript(invoiceLines, billingRun, billingAccount) : null;

        if (customInvoiceLinesGroups != null && !customInvoiceLinesGroups.isEmpty()) {
            customInvoiceLinesGroups.forEach(group -> this.writeInvoiceLines(group.getInvoiceLines(), group.getInvoiceKey()));
        } else {
            writeInvoiceLines(invoiceLines, null);
        }

        // Link Rated transactions with invoice lines
        // Mass update RT status and info
        String providerCode = currentUser.getProviderCode();
        final String schemaPrefix = providerCode != null ? EntityManagerProvider.convertToSchemaName(providerCode) + "." : "";

        // Choose a way of updating RT status - directly by a RT table update with a list of IDs as parameters
        // or via a pending table for a large number of RTs

        EntityManager em = getEntityManager();

        if (numberOrRts < minNrOfRtsToUsePendingTable) {

            // Need to flush, so RTs can be updated in mass
                em.flush();

            for (Object[] rtIlBrId : rtIlBrIds) {
                ratedTransactionService.linkRTsToIL((List<Long>) rtIlBrId[0], ((InvoiceLine) rtIlBrId[1]).getId(), (Long) rtIlBrId[2]);
            }

        } else {

            Session hibernateSession = em.unwrap(Session.class);
            hibernateSession.doWork(connection -> {
                try (PreparedStatement preparedStatement = connection.prepareStatement("insert into " + schemaPrefix + "billing_rated_transaction_pending (id, invoice_line_id, billing_run_id) values (?,?,?)")) {

                    int bi = 0;
					for (Object[] rtIlBrId : rtIlBrIds) {
						for (Long rtId : (List<Long>) rtIlBrId[0]) {
							preparedStatement.setLong(1, rtId);
							preparedStatement.setLong(2, ((InvoiceLine) rtIlBrId[1]).getId());
							if (rtIlBrId[2] != null) {
								preparedStatement.setLong(3, (Long) rtIlBrId[2]);
							} else {
								preparedStatement.setNull(3, java.sql.Types.NULL);
							}

							preparedStatement.addBatch();

							// Flush every 1M records
							if (bi > 0 && bi % 1000000 == 0) {
								preparedStatement.executeBatch();
							}
							bi++;
						}
					}

                    preparedStatement.executeBatch();

                } catch (SQLException e) {
                    log.error("Failed to insert into billing_rated_transaction_pending", e);
                    throw e;
            	}
            });
    
            // Need to flush, so RTs can be updated in mass
            em.flush();

            // Mass update RT with status and IL info
            em.createNamedQuery("RatedTransaction.massUpdateWithInvoiceLineInfoFromPendingTable" + (EntityManagerProvider.isDBOracle() ? "Oracle" : "")).executeUpdate();
            em.createNamedQuery("RatedTransaction.deletePendingTable").executeUpdate();

        }

        statistics.setCount(groupedRTs.size());
        statistics.setInvoiceLines(invoiceLines);

        return statistics;
    }

    /**
     * Build invoiceKey from an entity invoiceLine from different information such as billingAccount, seller, invoiceType, etc.
     *
     * @param invoiceLine InvoiceLine
     * @param billingRun 
     */
    private void buildInvoiceKey(InvoiceLine invoiceLine, BillingRun billingRun) {
    	
        StringJoiner invoiceKey =  new StringJoiner(UNDERSCORE_SEPARATOR);
        
        invoiceKey.add(formatEntityId(invoiceLine.getBillingAccount()));
        if (invoiceLine.getSeller() != null) {
            invoiceKey.add(formatId(invoiceLine.getSeller().getId()));
        }
        invoiceKey.add(formatId(invoiceLine.getInvoiceTypeId()));
        invoiceKey.add(formatId(invoiceLine.getPaymentMethodId()));invoiceKey.add(formatId(invoiceLine.getPaymentMethodId()));
        if(billingRun != null && billingRun.getBillingCycle() != null) {
            if(billingRun.getBillingCycle().getType()==BillingEntityTypeEnum.SUBSCRIPTION) {
                invoiceKey.add(formatEntityId(invoiceLine.getSubscription()));
            }
            if(billingRun.getBillingCycle().getType()==BillingEntityTypeEnum.ORDER) {
                invoiceKey.add(formatEntityId(invoiceLine.getCommercialOrder()));
            }
        }
        if (invoiceLine.getOpenOrderNumber() != null) {
            invoiceKey.add(invoiceLine.getOpenOrderNumber());
        }
        invoiceLine.setInvoiceKey(invoiceKey.toString());
    }
    
    private String formatEntityId(BaseEntity entity) {
    	return formatId(entity==null? null:entity.getId());
    }

	private String formatId(Long id) {
		return id==null?"":String.valueOf(id);
	}

    /**
     * method to execute billing cycle script.
     *
     * @param invoiceLines List<InvoiceLine> list of invoiceLines to be processed
     * @param billingRun List<InvoiceLine> billing run in used
     * @param billableEntity IBillableEntity entity to be billed
     * @return List<InvoiceLinesGroup> return the list of InvoiceLinesGroup defined by a customized script
     */
    private List<InvoiceLinesGroup> executeBillingCycleScript(List<InvoiceLine> invoiceLines, BillingRun billingRun, IBillableEntity billableEntity) {
        if (billingRun.getBillingCycle() != null && billingRun.getBillingCycle().getScriptInstance() != null
                && invoiceLines != null && !invoiceLines.isEmpty()) {
            InvoiceLine firstInvoiceLine = invoiceLines.get(0);
            InvoiceType invoiceType;

            if (firstInvoiceLine.getInvoiceTypeId() != null) {
                invoiceType = invoiceTypeService.findById(firstInvoiceLine.getInvoiceTypeId());
            } else {
                invoiceType = invoiceService.determineInvoiceType(false, false, false,
                        billingRun.getBillingCycle(), billingRun, firstInvoiceLine.getBillingAccount());
            }

            PaymentMethod paymentMethod;
            if (firstInvoiceLine.getPaymentMethodId() != null) {
                paymentMethod = paymentMethodService.findById(firstInvoiceLine.getPaymentMethodId());
            } else {
                paymentMethod = firstInvoiceLine.getBillingAccount().getPaymentMethod();
            }
            if (paymentMethod == null) {
                paymentMethod = firstInvoiceLine.getBillingAccount().getCustomerAccount().getPreferredPaymentMethod();
            }

            return invoiceService.executeBCScriptWithInvoiceLines(billingRun, invoiceType, invoiceLines, billableEntity,
                    billingRun.getBillingCycle().getScriptInstance().getCode(), paymentMethod);
        }
        return Collections.emptyList();
    }


    private void writeInvoiceLines(List<InvoiceLine> invoiceLines, String invoiceKey) {
        invoiceLines.forEach(invoiceLine -> {
            if(invoiceKey!=null) {
            	invoiceLine.setInvoiceKey(invoiceKey);
            }
            create(invoiceLine);
        });
    }

    public void deleteByBillingRun(long billingRunId) {
        List<Long> invoiceLinesIds = loadInvoiceLinesIdByBillingRun(billingRunId);
        if(!invoiceLinesIds.isEmpty()) {
            ratedTransactionService.detachRatedTransactions(invoiceLinesIds);
            getEntityManager()
                    .createNamedQuery("InvoiceLine.deleteByBillingRun")
                    .setParameter("billingRunId", billingRunId)
                    .executeUpdate();
        }
    }

    public List<Long> loadInvoiceLinesIdByBillingRun(long billingRunId) {
        return getEntityManager().createNamedQuery("InvoiceLine.listByBillingRun")
                .setParameter("billingRunId", billingRunId)
                .getResultList();
    }
    
    public List<Long> loadInvoiceLinesByBillingRunNotValidatedInvoices(long billingRunId) {
        return getEntityManager().createNamedQuery("InvoiceLine.listByBillingRunNotValidatedInvoices")
                .setParameter("billingRunId", billingRunId)
                .getResultList();
    }

	public void deleteInvoiceLines(BillingRun billingRun) {
		List<Long> invoiceLinesIds = loadInvoiceLinesByBillingRunNotValidatedInvoices(billingRun.getId());
        if(!invoiceLinesIds.isEmpty()) {
            ratedTransactionService.detachRatedTransactions(invoiceLinesIds);
            getEntityManager()
                    .createNamedQuery("InvoiceLine.deleteByBillingRunNotValidatedInvoices")
                    .setParameter("billingRunId", billingRun.getId())
                    .executeUpdate();
        }
		
	}
	
	@SuppressWarnings("unchecked")
    private void deleteByDiscountedPlan(InvoiceLine invoiceLine) {
        if (invoiceLine == null || invoiceLine.getId() == null) {
            return;
        }
        QueryBuilder queryBuilder = new QueryBuilder("from InvoiceLine il ", "il");
        queryBuilder.addCriterionEntity("il.discountedInvoiceLine", invoiceLine);
        Query query = queryBuilder.getQuery(getEntityManager());
        List<InvoiceLine> invoiceLines = query.getResultList();
        if(invoiceLines != null && !invoiceLines.isEmpty()) {
            var ids = invoiceLines.stream()
                    .map(InvoiceLine::getId)
                    .collect(toSet());
            remove(ids);
        }
    }

    public List<Long> getDiscountLines(Long id) {
        return getEntityManager()
                    .createNamedQuery("InvoiceLine.listDiscountLines")
                    .setParameter("invoiceLineId", id)
                    .getResultList();
    }

    public List<InvoiceLine> findByInvoiceAndIds(Invoice invoice, List<Long> invoiceLinesIds) {
        final int maxValue = getInstance().getPropertyAsInteger("database.number.of.inlist.limit", SHORT_MAX_VALUE);
        if (invoiceLinesIds.size() > maxValue) {
            List<InvoiceLine> invoiceLines = new ArrayList<>();
            List<List<Long>> invoiceLineIdsSubList = partition(invoiceLinesIds, maxValue);
            invoiceLineIdsSubList.forEach(subIdsList -> invoiceLines.addAll(loadBy(invoice, subIdsList)));
            return invoiceLines;
        } else {
            return loadBy(invoice, invoiceLinesIds);
        }
    }
    private List<InvoiceLine> loadBy(Invoice invoice, List<Long> invoiceLinesIds) {
        return getEntityManager().createNamedQuery("InvoiceLine.findByInvoiceAndIds", entityClass)
                .setParameter("invoice", invoice)
                .setParameter("invoiceLinesIds", invoiceLinesIds)
                .getResultList();
    }

    public void deleteByAssociatedInvoice(List<Long> invoices) {
        if(!invoices.isEmpty()) {
            List<Long> invoicesLines = findInvoiceLineByAssociatedInvoices(invoices);
            ratedTransactionService.getEntityManager()
                    .createNamedQuery("RatedTransaction.detachFromInvoices")
                    .setParameter("ids", invoices)
                    .setParameter("now", new Date())
                    .executeUpdate();
            if(!invoicesLines.isEmpty()) {
                ratedTransactionService.detachRatedTransactions(invoicesLines);
                getEntityManager()
                        .createQuery("DELETE FROM InvoiceLine WHERE id in (:ids)")
                        .setParameter("ids", invoicesLines)
                        .executeUpdate();
            }
        }
    }

    public List<Long> findInvoiceLineByAssociatedInvoices(List<Long> invoiceIds) {
        if(invoiceIds.isEmpty()) {
            return Collections.emptyList();
        }
        return getEntityManager().createNamedQuery("InvoiceLine.listByAssociatedInvoice")
                    .setParameter("invoiceIds", invoiceIds)
                    .getResultList();
    }

    /**
     * Get tax details for a given tax
     * @param mainTax
     * @param taxAmount
     * @param convertedTaxAmount
     * @return Optional TaxDetails
     */
    public Optional<TaxDetails> getTaxDetails(Tax mainTax, BigDecimal taxAmount, BigDecimal convertedTaxAmount) {
        TaxDetails taxDetails;
        if(mainTax == null) {
            taxDetails = null;
        } else if (mainTax.isComposite()) {
            mainTax = taxService.findById(mainTax.getId(), true);
            taxDetails = TaxDetails.fromTax(mainTax, taxAmount, convertedTaxAmount);
        } else {
            taxDetails = new TaxDetails(mainTax.getId(), mainTax.getCode(), mainTax.getDescription(),
                    mainTax.getPercent(), taxAmount, convertedTaxAmount, null, FALSE);
        }
        return ofNullable(taxDetails);
    }

    /**
     * Invoiced amount by open order and billing account
     * @param openOrderNumber open order number
     * @param billingAccountId billing account id
     * @return  invoiced amount
     */
    public BigDecimal invoicedAmountByOpenOrder(String openOrderNumber, Long billingAccountId) {
        try {
            BigDecimal invoicedAmount =  (BigDecimal) getEntityManager().createNamedQuery("InvoiceLine.sumAmountByOpenOrderNumberAndBA")
                                                        .setParameter("openOrderNumber", openOrderNumber)
                                                        .setParameter("billingAccountId", billingAccountId)
                                                        .getSingleResult();
            return invoicedAmount != null ? invoicedAmount : BigDecimal.ZERO;
        } catch (NoResultException exception) {
            return BigDecimal.ZERO;
        }
    }
    
    public List<Object[]> findMinimumsTocheck(BillingRun billingRun, String joinPath) {
		if (joinPath == null || !joinPath.contains("mt")) {
			return Collections.emptyList();
		}
		String amount = appProvider.isEntreprise() ? " sum(il.amountWithoutTax) " : " sum(il.amountWithTax) ";
		String baseQuery = "SELECT mt, ba, " + amount + " FROM InvoiceLine il join il.billingAccount ba " + joinPath + " WHERE il.status='OPEN' AND il.billingRun.id=:billingRunId AND mt.minimumAmountEl is not null GROUP BY mt, ba HAVING (mt.minimumAmountEl like '#{%' OR cast(mt.minimumAmountEl AS big_decimal)>"+ amount + ")";

		QueryBuilder qb = new QueryBuilder(baseQuery);
		final List<Object[]> resultList = qb.getQuery(getEntityManager()).setParameter("billingRunId", billingRun.getId()).getResultList();
		return resultList;
	}

	
    @JpaAmpNewTx
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void createMinInvoiceLine(BillingRun bilingRun, AccountingArticle defaultMinAccountingArticle, Object[] minimumData) {
		IInvoicingMinimumApplicable minEntity = (IInvoicingMinimumApplicable)minimumData[0];
		BillingAccount billingAccount = (BillingAccount)minimumData[1];
		BigDecimal amount = (BigDecimal)minimumData[2];
		Seller seller = minEntity.getSeller();
		if (seller == null) {
		    throw new BusinessException("Default Seller is mandatory for invoice minimum (Customer.seller)");
		}
		BigDecimal minAmount = minAmountService.evaluateMinAmountExpression(minEntity.getMinimumAmountEl(), minEntity);
		String minAmountLabel = minAmountService.evaluateMinAmountLabelExpression(minEntity.getMinimumLabelEl(), minEntity);
		BigDecimal diff = minAmount.subtract(amount);
		if (diff.compareTo(BigDecimal.ZERO) <= 0) {
		    return;
		}
		
		if (defaultMinAccountingArticle == null) {
		    log.error("No default AccountingArticle defined");
		    return;
		}
		TaxMappingService.TaxInfo taxInfo = taxMappingService.determineTax(defaultMinAccountingArticle.getTaxClass(), seller, billingAccount, null, bilingRun.getInvoiceDate(), null, true, false, null);
		InvoiceLine invoiceLine = createInvoiceLine((!StringUtils.isBlank(minAmountLabel)?minAmountLabel:defaultMinAccountingArticle.getDescription()), billingAccount, billingAccount, bilingRun.getInvoiceDate(), minEntity, seller, defaultMinAccountingArticle, taxInfo, diff);
		invoiceLine.setBillingRun(bilingRun);
		create(invoiceLine);
	}

    public List<InvoiceLine> findByIdsAndAdjustmentStatus(List<Long> invoiceLinesIds, AdjustmentStatusEnum adjustmentStatusEnum) {
        return getEntityManager().createNamedQuery("InvoiceLine.findByIdsAndAdjustmentStatus", entityClass)
                .setParameter("status", adjustmentStatusEnum.toString())
                .setParameter("invoiceLinesIds", invoiceLinesIds)
                .getResultList();
    }
    
    public List<InvoiceLine> findByIdsAndOtherAdjustmentStatus(List<Long> invoiceLinesIds) {
        return getEntityManager().createNamedQuery("InvoiceLine.findByIdsAndOtherAdjustmentStatus", entityClass)
                .setParameter("status", AdjustmentStatusEnum.NOT_ADJUSTED.toString())
                .setParameter("invoiceLinesIds", invoiceLinesIds)
                .getResultList();
    }
    
    public List<InvoiceLine> findInvoiceLinesToAdjust() {
        return getEntityManager().createNamedQuery("InvoiceLine.findByAdjustmentStatus", entityClass)
                .setParameter("status", AdjustmentStatusEnum.TO_ADJUST.toString())
                .getResultList();
    }
    
    public List<InvoiceLine> findByIdsAndInvoiceType(List<Long> invoiceLinesIds, String invoiceType) {
        return getEntityManager().createNamedQuery("InvoiceLine.findByIdsAndInvoiceType", entityClass)
                .setParameter("invoiceLinesIds", invoiceLinesIds)
                .setParameter("invoiceType", invoiceType)
                .getResultList();
    }
    
    public void updateForAdjustment(Collection<Long> invoicesIds, AdjustmentStatusEnum status) {
        getEntityManager().createNamedQuery("InvoiceLine.updateForAdjustment")
                .setParameter("status", status.toString())
                .setParameter("now", new Date())
                .setParameter("ids", invoicesIds)
                .executeUpdate();
    }
    
    public List<Date> getCustomSubscriptionAge(Long invoiceId, String referenceDate) {
    	String query = "select distinct referenceDate from InvoiceLine il where il.invoice.id=:id";
        return getEntityManager().createQuery(query.replace("referenceDate", referenceDate), Date.class)
        		.setParameter("id", invoiceId)
        		.getResultList();
    }

    public List<Object[]> getTotalDiscountAmountByBR(BillingRun billingRun) {
		return getEntityManager().createNamedQuery("InvoiceLine.sumAmountsDiscountByBillingAccount")
				.setParameter("billingRunId", billingRun.getId()).getResultList();
	}

    @SuppressWarnings("unchecked")
    public List<Object[]> getMaxIlAmountAdj(Long id) {
        return getEntityManager().createNamedQuery("InvoiceLine.getMaxIlAmountAdj").setParameter("invoiceId", id).getResultList();
    }

    public List<BillingAccount> findBillingAccountsBy(List<Long> invoiceLinesIds) {
        final int maxValue = getInstance().getPropertyAsInteger("database.number.of.inlist.limit", SHORT_MAX_VALUE);
        if (invoiceLinesIds.size() > maxValue) {
            List<BillingAccount> billingAccounts = new ArrayList<>();
            List<List<Long>> invoiceLineIdsSubList = partition(invoiceLinesIds, maxValue);
            invoiceLineIdsSubList.forEach(subIdsList -> billingAccounts.addAll(loadBillingAccounts(subIdsList)));
            return new ArrayList<>(new HashSet<>(billingAccounts));
        } else {
            return loadBillingAccounts(invoiceLinesIds);
        }
    }
    private List<BillingAccount> loadBillingAccounts(List<Long> invoiceLinesIds) {
        return getEntityManager()
                .createNamedQuery("InvoiceLine.BillingAccountByILIds")
                .setParameter("ids", invoiceLinesIds)
                .getResultList();
    }

    private BillingAccount getBillingAccount(Invoice invoice, InvoiceLine invoiceLine) {
        BillingAccount billingAccount = invoiceLine.getBillingAccount();
        if (invoice != null) {
            billingAccount = invoice.getBillingAccount();
        }
        return billingAccount;
    }

    private Seller getSeller(Invoice invoice, InvoiceLine invoiceLine, BillingAccount billingAccount) {
        Seller seller = null;
        if (invoice != null) {
            seller = invoice.getSeller();
        }
        if (seller == null && invoiceLine.getCommercialOrder() != null) {
            seller = invoiceLine.getCommercialOrder().getSeller();
        }
        if (seller == null) {
            seller = billingAccount.getCustomerAccount().getCustomer().getSeller();
        }
        return seller;
    }

    private void addDiscountPlanInvoice(InvoiceLine invoiceLine, DiscountPlan discountPlan) {
        if (discountPlan != null && invoiceLine.getAmountWithoutTax() != null && invoiceLine.getAmountWithoutTax().compareTo(BigDecimal.ZERO) > 0) {
            Invoice invoice = invoiceLine.getInvoice();
            BillingAccount billingAccount = getBillingAccount(invoice, invoiceLine);
            Seller seller = getSeller(invoice, invoiceLine, billingAccount);
            addDiscountPlanInvoice(discountPlan, invoiceLine, billingAccount, invoice, invoiceLine.getAccountingArticle(), seller);
        }
    }

    private void updateDsicountPlan(InvoiceLine invoiceLine, org.meveo.apiv2.billing.InvoiceLine resource, DiscountPlan oldDiscountPlan) {
        if (invoiceLine.getInvoice() != null && invoiceLine.getInvoice().getStatus() != VALIDATED) {
            String oldDiscountPlanCode = oldDiscountPlan != null ? oldDiscountPlan.getCode() : null;
            // if there is a new discount plan (it can be null also), remove the discount invoice line if exists.
            if (!StringUtils.isBlank(oldDiscountPlanCode) && !oldDiscountPlanCode.equalsIgnoreCase(resource.getDiscountPlanCode())) {
                List<Long> discountedInvoiceLineIds = getDiscountLines(invoiceLine.getId());
                if (!discountedInvoiceLineIds.isEmpty()) {
                    remove(discountedInvoiceLineIds.stream().collect(Collectors.toSet()));
                    invoiceLine.setDiscountAmount(BigDecimal.ZERO);
                }
                invoiceLine.setDiscountPlan(null);
            }
            // add the new discount plan.
            if (!StringUtils.isBlank(resource.getDiscountPlanCode()) && !resource.getDiscountPlanCode().equalsIgnoreCase(oldDiscountPlanCode)) {
                DiscountPlan newDiscountPlan = (DiscountPlan) tryToFindByEntityClassAndCode(DiscountPlan.class, resource.getDiscountPlanCode());
                invoiceLine.setDiscountPlan(newDiscountPlan);
                addDiscountPlanInvoice(invoiceLine, newDiscountPlan);
            }
        }
    }

    /**
     * Update Invoice Line
     *
     * @param invoiceLine  Invoice Line to update {@link InvoiceLine}
     * @param resource     Invoice Line input {@link org.meveo.apiv2.billing.InvoiceLine}
     * @param discountPlan Invoice Line dsicount plan {@link DiscountPlan}
     */
    public void updateInvoiceLine(InvoiceLine invoiceLine, org.meveo.apiv2.billing.InvoiceLine resource, DiscountPlan discountPlan) {
        updateDsicountPlan(invoiceLine, resource, discountPlan);
        update(invoiceLine);
    }

    /**
     * Gets the sum of invoice lines amounts per a provided billing run.
     *
     * @param billingRunId the billing run id
     * @return list of array object
     */
    public List<Object[]> sumAmountsPerBR(Long billingRunId) {
        return getEntityManager().createNamedQuery("InvoiceLine.sumAmountsPerBR")
                .setParameter("billingRunId", billingRunId)
                .getResultList();
    }

    /**
     * Statistics for Invoice line creation process
     */
    public class InvoiceLineCreationStatistics extends BasicStatistics {

        List<InvoiceLine> invoiceLines;

        /**
         * @return Invoice lines created
         */
        public List<InvoiceLine> getInvoiceLines() {
            return invoiceLines;
        }

        /**
         * @param invoiceLines Invoice lines
         */
        public void setInvoiceLines(List<InvoiceLine> invoiceLines) {
            this.invoiceLines = invoiceLines;
        }
    }

    /**
     * Bridge Invoice lines that correspond to discount type rated transactions with invoices lines that they discount<BR/>
     * Note: Each discount type Rated Transaction correspond to a single Invoice line
     *
     * @param billingRunId Billing run Id
     * @param minId        Minimum Rated transaction Id
     * @param maxId        Maximum Rated transaction Id
     */
    public void bridgeDiscountILs(Long billingRunId, Long minId, Long maxId) {

        getEntityManager().createNamedQuery("InvoiceLine.massUpdateWithDiscountedIL" + (EntityManagerProvider.isDBOracle() ? "Oracle" : "")).setParameter("brId", billingRunId).setParameter("minId", minId)
                .setParameter("maxId", maxId).executeUpdate();
    }

    public List<Object[]> getInvoiceLineStatistics(Long billingRunId) {
        return getEntityManager().createNamedQuery("InvoiceLine.getInvoiceLinesStatistics")
                .setParameter("billingRunId", billingRunId)
                .getResultList();
    }

    /**
     * Check threshold by invoice
     *
     * @param billingRunId       the billingRun Id
     * @param invoicingThreshold the invoicing threshold
     * @param checkThreshold     check threshold
     * @param isEntreprise
     * @return list of array of billing accounts Ids and invoice lines Ids
     */
    public List<Object[]> checkThreshold(Long billingRunId, BigDecimal invoicingThreshold, ThresholdOptionsEnum checkThreshold, boolean isEntreprise) {
        if (StringUtils.isBlank(billingRunId) || invoicingThreshold == null || checkThreshold == null) {
            return Collections.emptyList();
        }
        String businessType = isEntreprise?"B2B":"B2C";
        try {
            return getEntityManager().createNamedQuery("InvoiceLine.checkThreshold"+businessType)
                    .setParameter("billingRunId", billingRunId)
                    .setParameter("invoicingThreshold", invoicingThreshold)
                    .setParameter("checkThreshold", checkThreshold.toString())
                    .getResultList();
        } catch (NoResultException e) {
            log.warn("checkThreshold"+businessType+" : no result found for the provided billingRun = " + billingRunId);
            return emptyList();
        }
    }

    /**
     * Check threshold 
     *
     * @param billingRunId the billingRun Id
     * @param isEntreprise
     * @param checkLevel ('ByCA','ByCA', 'ByBA')
     * @return billing accounts Ids
     */
    public List<Object> checkThreshold(Long billingRunId, boolean isEntreprise, String checkLevel) {
        if (StringUtils.isBlank(billingRunId)) {
            return Collections.emptyList();
        }
        String businessType = isEntreprise?"B2B":"B2C";
        try {
            return getEntityManager().createNamedQuery("InvoiceLine.checkThreshold"+businessType+checkLevel)
                    .setParameter("billingRunId", billingRunId)
                    .getResultList();
        } catch (NoResultException e) {
            log.warn("checkThreshold"+businessType+checkLevel+" : no billing account found for the provided billingRun = " + billingRunId);
            return emptyList();
        }
    }

    /**
     * Cancel the provided invoice lines
     *
     * @param invoiceLinesIds the invoice lines Ids
     */
    public void cancelInvoiceLines(Collection<Long> invoiceLinesIds) {
        if (invoiceLinesIds == null || invoiceLinesIds.isEmpty()) {
            return;
        }
        getEntityManager().createNamedQuery("InvoiceLine.cancelInvoiceLines")
                .setParameter("now", new Date())
                .setParameter("ids", invoiceLinesIds)
                .executeUpdate();
    }

    /**
     * Cancel invoice lines of provided billingRun and billing accounts
     *
     * @param billingRunId       the billingRun Id
     * @param billingAccountsIds billing accounts ids
     */
    public void cancelInvoiceLines(Long billingRunId, Collection<Long> billingAccountsIds) {
        if (billingRunId == null || billingAccountsIds == null || billingAccountsIds.isEmpty()) {
            return;
        }
        getEntityManager().createNamedQuery("InvoiceLine.cancelInvoiceLinesByBRandBAs")
                .setParameter("now", new Date())
                .setParameter("billingRunId", billingRunId)
                .setParameter("baIds", billingAccountsIds)
                .executeUpdate();
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void calculateTax(List<Long> invoiceLineIds, JobExecutionResultImpl jobExecutionResult) {
        List<InvoiceLine> invoiceLines = getEntityManager().createQuery("SELECT il FROM InvoiceLine il WHERE il.id IN :ids", InvoiceLine.class).setParameter("ids", invoiceLineIds).getResultList();
		Map<String, List<Long>> errorsMap = new HashMap<>();
		int toProcess=invoiceLineIds.size();
		invoiceLines.stream().forEach(invoiceLine -> {
		    try {
				TaxInfo taxInfo = taxMappingService.determineTax(invoiceLine.getAccountingArticle().getTaxClass(), invoiceLine.getSeller(),
						invoiceLine.getBillingAccount(), invoiceLine.getUserAccount(), invoiceLine.getBillingRun().getInvoiceDate(), null, Boolean.TRUE.equals(invoiceLine.getBillingAccount().isExoneratedFromtaxes()), false, invoiceLine.getTax());
				calculateTaxAmounts(invoiceLine, taxService.findById(taxInfo.tax.getId()), invoiceLine.getAmountWithoutTax());
		    } catch (Exception e) {
		        errorsMap.computeIfAbsent(e.getMessage(), k -> new ArrayList<>()).add(invoiceLine.getId());
		    }
		});
		errorsMap.forEach((key, value) ->reportErrors(jobExecutionResult, key, value, toProcess));
	}

    public void calculateTaxAmounts(InvoiceLine invoiceLine, Tax tax, BigDecimal amount) {
		BigDecimal[] amounts = NumberUtils.computeDerivedAmounts(amount, amount, 
            tax.getPercent(), appProvider.isEntreprise(), appProvider.getInvoiceRounding(),
            appProvider.getInvoiceRoundingMode().getRoundingMode());
        invoiceLine.setAmountWithoutTax(amounts[0]);
        invoiceLine.setAmountWithTax(amounts[1]);
        invoiceLine.setAmountTax(amounts[2]);
        invoiceLine.setTax(tax);
        invoiceLine.setTaxRate(tax.getPercent());
	}
    
	private void reportErrors(JobExecutionResultImpl jobExecutionResult, String key, List<Long> value, int toProcess) {
		int errorsToCompute = value.size();
		errorsToCompute = errorsToCompute == toProcess || errorsToCompute == 0 ? 0 : errorsToCompute;
		jobExecutionResult.registerError("" + value.size() + " errors of: " + key + ", IDs: " + value.stream().map(String::valueOf).collect(Collectors.joining(", ")), errorsToCompute);
		jobExecutionResult.addNbItemsCorrectlyProcessed(-errorsToCompute);
	}

}