package org.meveo.service.billing.impl;

import org.meveo.model.billing.BillingAccountAmount;
import org.meveo.service.base.PersistenceService;

import javax.ejb.Stateless;

@Stateless
public class BillingAccountAmountService extends PersistenceService<BillingAccountAmount> {
}
