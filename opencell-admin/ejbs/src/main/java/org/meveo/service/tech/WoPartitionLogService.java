package org.meveo.service.tech;

import org.meveo.model.admin.partitioning.WoPartitionLog;
import org.meveo.service.base.PersistenceService;

import javax.ejb.Stateless;

@Stateless
public class WoPartitionLogService extends PersistenceService<WoPartitionLog> {
}
