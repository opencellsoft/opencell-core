/*
 * (C) Copyright 2015-2020 Opencell SAS (https://opencellsoft.com/) and contributors.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW. EXCEPT WHEN
 * OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS
 * IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE ENTIRE RISK AS TO
 * THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU. SHOULD THE PROGRAM PROVE DEFECTIVE,
 * YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.
 *
 * For more information on the GNU Affero General Public License, please consult
 * <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */
package org.meveo.service.admin.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.security.DeclareRoles;
import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.NoResultException;

import org.keycloak.representations.idm.UserRepresentation;
import org.meveo.admin.exception.BusinessException;
import org.meveo.admin.exception.ElementNotFoundException;
import org.meveo.admin.exception.InvalidParameterException;
import org.meveo.admin.exception.UsernameAlreadyExistsException;
import org.meveo.admin.util.pagination.PaginationConfiguration;
import org.meveo.api.exception.EntityAlreadyExistsException;
import org.meveo.commons.utils.ParamBeanFactory;
import org.meveo.commons.utils.StringUtils;
import org.meveo.model.admin.User;
import org.meveo.security.client.KeycloakAdminClientService;
import org.meveo.service.base.PersistenceService;

/**
 * User service implementation.
 */
@Stateless
@DeclareRoles({ "userManagement", "userSelfManagement", "apiUserManagement", "apiUserSelfManagement" })
public class UserService extends PersistenceService<User> {

    static User systemUser = null;

    @Inject
    KeycloakAdminClientService keycloakAdminClientService;

    @Inject
    protected ParamBeanFactory paramBeanFactory;
    
    /**
     * Identifies the source of user and role information
     * 
     * @author Andrius Karpavicius
     */
    public enum UserManagementMasterEnum {

        /**
         * The primary source of users and roles is Opencell
         */
        OPENCELL,

        /**
         * The primary source of users and roles is Keycloak.<br/>
         * Opencell stores only custom field and secured entity information, records in adm_user nor adm_role table are not required.<br/>
         * When Users and roles are managed from Opencell API/GUI side, and user and role information <b>is synchronized</b> to Keycloak
         */
        KC,

        /**
         * The <b>ONLY</b> source of users and roles is Keycloak.<br/>
         * Opencell stores only custom field and secured entity information, records in adm_user nor adm_role table are not required.<br/>
         * When Users and roles are managed from Opencell API/GUI side, and user and role information is <b>NOT synchronized</b> to Keycloak
         */
        KC_USER_READ_ONLY;

    }

    @Override
    @RolesAllowed({ "userManagement", "userSelfManagement", "apiUserManagement", "apiUserSelfManagement" })
    public void create(User user) throws UsernameAlreadyExistsException, InvalidParameterException {
        user.setUserName(user.getUserName().toUpperCase());
        String userId =keycloakAdminClientService.createUser(user.getUserName(), user.getName().getFirstName(), user.getName().getLastName(), user.getEmail(), user.getPassword(), user.getUserLevel(), user.getRoles(), null);
	    // check if the user already exists
        if (findByUsername(user.getUserName(), false, false, false) != null) {
        	if(userId==null) // when master=OC and user already exists in KC
        		throw new EntityAlreadyExistsException(User.class, user.getUserName(), "username");
        }else {
        	super.create(user);
        }
	    
    }

    @Override
    @RolesAllowed({ "userManagement", "userSelfManagement", "apiUserManagement", "apiUserSelfManagement" })
    public User update(User user) throws ElementNotFoundException, InvalidParameterException {
        user.setUserName(user.getUserName().toUpperCase());
		keycloakAdminClientService.updateUser(user.getUserName(), user.getName().getFirstName(), user.getName().getLastName(), user.getEmail(), user.getPassword(), user.getUserLevel(), user.getRoles(), null);
        if(user.getId() != null) {
            return super.update(user);
        } else {
            return user;
        }
    }

    @RolesAllowed({ "userManagement", "userSelfManagement", "apiUserManagement", "apiUserSelfManagement" })
    public void updateUserWithAttributes(User user, Map<String, String> attributes) throws ElementNotFoundException, InvalidParameterException {
        user.setUserName(user.getUserName().toUpperCase());
		String firstName = user.getName() != null ? user.getName().getFirstName() : null;
		String lastName = user.getName() != null ? user.getName().getLastName() : null;
		keycloakAdminClientService.updateUser(user.getUserName().toUpperCase(), firstName, lastName, user.getEmail(), user.getPassword(), user.getUserLevel(), user.getRoles(), attributes);
		 if(user.getId() != null) {
            super.update(user);
        }
    }

    @Override
    @RolesAllowed({ "userManagement", "apiUserManagement" })
    public void remove(User user) throws BusinessException {
        keycloakAdminClientService.deleteUser(user.getUserName());
        super.remove(user);
    }

    /**
     * Lookup a user by a username. NOTE: Does not create a user record in Opencell if user already exists in Keycloak
     * 
     * @param username Username to lookup by
     * @param extendedInfo Shall group membership and roles be retrieved
     * @return User found
     */
    public User findByUsername(String username, boolean extendedInfo) {
        return findByUsername(username, extendedInfo, false, false);
    }

    /**
     * Lookup a user by a username
     * 
     * @param username Username to lookup by
     * @param extendedInfo Shall group membership and roles be retrieved
     * @param syncWithKC Shall a user record be created in Opencell if a user already exists in Keycloak
     * @return User found
     */
    public User findByUsername(String username, boolean extendedInfo, boolean syncWithKC, boolean extendedClientRoles) {
        User lUser = null;
	    
	    if(canSynchroWithKC()) {
		    lUser = keycloakAdminClientService.findUser(username, extendedInfo, extendedClientRoles);
        }
		if(lUser == null) {
			lUser = getUserFromDatabase(username);
			
			if (lUser != null && extendedInfo) {
				this.fillKeycloakUserInfo(lUser);
			}
		}
        return lUser;
    }

    public User getUserFromDatabase(String pUserName) {
        User lUser = null;
        try {
            lUser = getEntityManager().createNamedQuery("User.getByUsername", User.class).setParameter("username", pUserName.toLowerCase()).getSingleResult();
        } catch (NoResultException ex) {
            //ADD Log
        }

        return lUser;
    }

    @Override
    public List<User> list(PaginationConfiguration config) {
        List<User> users = new ArrayList<>();
	    if(!canSynchroWithKC()) {
            String firstName = (String) config.getFilters().get("name.firstName");
            String lastName = (String) config.getFilters().get("name.lastName");
            String email = (String) config.getFilters().get("email");

            if(StringUtils.isBlank(firstName)) {
                this.removeFilters(config, "name.firstName");
            }

            if(StringUtils.isBlank(lastName)) {
                this.removeFilters(config, "name.lastName");
            }

            if(StringUtils.isBlank(email)) {
                this.removeFilters(config, "email");
            }

            users = super.list(config);
            users.forEach(this::fillKeycloakUserInfo);
        } else {
            //Get user from keycloak
            users = keycloakAdminClientService.listUsers(config);
            this.removeFilters(config, "name.firstName", "name.lastName", "email");

            //Construct a list of names
            List<String> usernamesList = users.stream().map(User::getUserName).collect(Collectors.toList());
            config.getFilters().put("inList userName", usernamesList);

            //Get list of users from database and fill all fields
            List<User> lDbUsers = super.list(config);
            users.forEach(keycloakUser -> {
                lDbUsers.forEach(dbUser -> {
                    if(keycloakUser.getUserName().equalsIgnoreCase(dbUser.getUserName())) {
                        fillEmptyFields(keycloakUser, dbUser);
                    }
                });
            });
        }

        return users;
    }

    @Override
    public long count(PaginationConfiguration config) {
        List<User> users;
	    if(!canSynchroWithKC()) {
            String firstName = (String) config.getFilters().get("name.firstName");
            String lastName = (String) config.getFilters().get("name.lastName");
            String email = (String) config.getFilters().get("email");

            if(StringUtils.isBlank(firstName) && StringUtils.isBlank(lastName) && StringUtils.isBlank(email)) {
                this.removeFilters(config, "name.firstName", "name.lastName", "email");
                return super.count(config);
            } else {
                return super.count(config);
            }
        } else {
            users = keycloakAdminClientService.listUsers(config);
            return users.size();
        }
    }

    /**
     * Check if user belongs to a group or a higher group
     * 
     * @param belongsToUserGroup A group to check
     * @return True if user belongs to a given group of to a parent of the group
     */
    public boolean isUserBelongsGroup(String belongsToUserGroup) {
        // TODO finish checking the hierarchy
        return belongsToUserGroup.equalsIgnoreCase(currentUser.getUserGroup());
    }

    public UserRepresentation getUserRepresentationByUsername(String username) throws ElementNotFoundException {
	    if(canSynchroWithKC()) {
            return keycloakAdminClientService.getUserRepresentationByUsername(username);
        } else {
            return null;
        }
    }

    /**
     * Lookup a user by an id
     *
     * @param extendedInfo Shall group membership and roles be retrieved
     * @return User found
     */
    @Override
    public User findById(Long id, boolean extendedInfo) {
        if(id == null) {
            return null;
        }

        User user = findById(id);
        if (user == null) {
             return null;
        }

        if(extendedInfo) {
            this.fillKeycloakUserInfo(user);
        }

        return user;
    }

    /**
     * Lookup a keycloak user in database
     *
     * @param kcUser keycloak user to lookup by
     * @return User found
     */
    private User findKeycloakUser(User kcUser) {
        User user = null;
        try {
            user = getEntityManager().createNamedQuery("User.getByUsername", User.class).setParameter("username", kcUser.getUserName().toLowerCase()).getSingleResult();
        } catch (NoResultException ex) {
            user = new User();
            // Set fields, even they are transient, so they can be used in a notification if any is fired upon user creation
            user.setEmail(kcUser.getEmail());
            user.setName(kcUser.getName());
            user.setRoles(kcUser.getRoles());
            user.setUserLevel(kcUser.getUserLevel());
            user.setUserName(kcUser.getUserName());
            super.create(user);
        }

        user.setEmail(kcUser.getEmail());
        user.setName(kcUser.getName());
        user.setRoles(kcUser.getRoles());
        user.setUserLevel(kcUser.getUserLevel());
        return user;
    }

    /**
     * Lookup a keycloak user info
     *
     * @param user user
     */
    private void fillKeycloakUserInfo(User user) {
        User kcUser = keycloakAdminClientService.findUser(user.getUserName(), true, false);
        if (kcUser != null) {
            user.setRoles(kcUser.getRoles());
            user.setUserLevel(kcUser.getUserLevel());
        }
    }

    /**
     * Remove filters from config
     *
     * @param pConfig {@link PaginationConfiguration}
     * @param pKeys A list of keys to remove
     */
    private void removeFilters(PaginationConfiguration pConfig, String ... pKeys) {
        for(String key : pKeys) {
            pConfig.getFilters().remove(key);
        }
    }

    /**
     * Fill Empty field to return it
     * @param pKeycloakUser {@link User} User returned from keycloak
     * @param pDbUser {@link User} User returned from database
     */
    private void fillEmptyFields(User pKeycloakUser, User pDbUser) {
        if(pKeycloakUser.getEmail() == null && pDbUser.getEmail() != null && !pDbUser.getEmail().isBlank()) {
            pKeycloakUser.setEmail(pDbUser.getEmail());
        }

        if(pKeycloakUser.getUuid() == null && pDbUser.getUuid() != null && !pDbUser.getUuid().isEmpty()) {
            pKeycloakUser.setUuid(pDbUser.getUuid());
        }
    }

    public String getUserAttributeValue(String username, String attributeName) {
        UserRepresentation userRepresentation = getUserRepresentationByUsername(username);
        if (userRepresentation != null) {
            Map<String, List<String>> keycloakAttributes = userRepresentation.getAttributes();

            if (keycloakAttributes != null && !keycloakAttributes.isEmpty()) {
                for (Map.Entry<String, List<String>> entry : keycloakAttributes.entrySet()) {
                    if (entry.getKey().equalsIgnoreCase(attributeName)) {
                        return String.join(", ", entry.getValue());
                    }
                }
            }
        }
        return null;
    }
	
	public boolean canSynchroWithKC(){
		String lUserManagementSource = paramBeanFactory.getInstance().getProperty("userManagement.master", "KC");
		return lUserManagementSource.equalsIgnoreCase("KC");
	}
}