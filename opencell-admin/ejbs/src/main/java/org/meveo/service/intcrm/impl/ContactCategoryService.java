package org.meveo.service.intcrm.impl;

import javax.ejb.Stateless;

import org.meveo.model.communication.contact.ContactCategory;
import org.meveo.service.base.BusinessService;

@Stateless
public class ContactCategoryService extends BusinessService<ContactCategory> {

}
