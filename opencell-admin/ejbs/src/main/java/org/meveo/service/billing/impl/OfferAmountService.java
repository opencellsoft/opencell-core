package org.meveo.service.billing.impl;

import org.meveo.model.billing.OfferAmount;
import org.meveo.service.base.PersistenceService;

import javax.ejb.Stateless;

@Stateless
public class OfferAmountService extends PersistenceService<OfferAmount> {
}
