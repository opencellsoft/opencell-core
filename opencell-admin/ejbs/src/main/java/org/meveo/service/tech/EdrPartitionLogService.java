package org.meveo.service.tech;

import org.meveo.model.admin.partitioning.EdrPartitionLog;
import org.meveo.service.base.PersistenceService;

import javax.ejb.Stateless;

@Stateless
public class EdrPartitionLogService extends PersistenceService<EdrPartitionLog> {
}
