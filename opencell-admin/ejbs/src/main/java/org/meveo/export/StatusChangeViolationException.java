package org.meveo.export;

public class StatusChangeViolationException extends Exception{

    public StatusChangeViolationException(String message) {
        super(message);
    }
}
