package org.meveo.admin.util.pagination;

public enum FilterOperatorEnum {
	OR, AND;
}
