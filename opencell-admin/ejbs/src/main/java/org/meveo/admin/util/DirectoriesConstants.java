package org.meveo.admin.util;

/**
 * Constants
 * 
 * @author Anas
 *
 */
public class DirectoriesConstants {
	
	/**
	 * Reject subfolder
	 */
	public static final String REJECT_SUBFOLDER = "reject";

	/**
	 * Warning Subfolder
	 */
	public static final String WARNINGS_SUBFOLDER = "warnings";

	/**
	 * Errors Subfolder
	 */
	public static final String ERRORS_SUBFOLDER = "errors";

	/**
	 * Output Subfolder
	 */
	public static final String OUTPUT_SUBFOLDER = "output";

	/**
	 * Input Subfolder
	 */
	public static final String INPUT_SUBFOLDER = "input";

	/**
	 * Import Root Folder
	 */
	public static final String IMPORTS_ROOT_FOLDER = "imports";

	/**
	 * Invoices Root Folder
	 */
	public static final String INVOICES_ROOT_FOLDER = "invoices";

	/**
	 * jasper Root Folder
	 */
	public static final String JASPER_ROOT_FOLDER = "jasper";
	
	
	/**
	 * jasper Root Folder
	 */
	public static final String INVOICE_WRITEOF_ARCHIVE_ROOT_FOLDER = "archives";

}
