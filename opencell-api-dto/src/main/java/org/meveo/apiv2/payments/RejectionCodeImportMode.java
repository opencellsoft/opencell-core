package org.meveo.apiv2.payments;

public enum RejectionCodeImportMode {
    REPLACE, UPDATE
}
