package org.meveo.apiv2.payments;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;
import org.meveo.apiv2.models.Resource;

import javax.annotation.Nullable;

@Value.Immutable
@Value.Style(jdkOnly = true)
@JsonDeserialize(as = ImmutableRejectionCodeClearInput.class)
public interface RejectionCodeClearInput {

    @Nullable
    Resource getPaymentGateway();

    @Nullable
    @Value.Default
    default Boolean getForce() {
        return Boolean.FALSE;
    }
}
