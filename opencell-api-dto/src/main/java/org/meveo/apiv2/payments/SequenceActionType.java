package org.meveo.apiv2.payments;

public enum SequenceActionType {
    UP, DOWN
}
