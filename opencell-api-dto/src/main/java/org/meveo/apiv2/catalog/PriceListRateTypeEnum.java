package org.meveo.apiv2.catalog;

public enum PriceListRateTypeEnum {
    PERCENTAGE, FIXED;
}
