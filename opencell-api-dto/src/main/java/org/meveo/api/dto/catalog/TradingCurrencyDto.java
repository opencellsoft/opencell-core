package org.meveo.api.dto.catalog;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import org.meveo.api.dto.BusinessEntityDto;

@XmlAccessorType(XmlAccessType.FIELD)
public class TradingCurrencyDto extends BusinessEntityDto {

	private static final long serialVersionUID = 6733599446700596337L;

}
